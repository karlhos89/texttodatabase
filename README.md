----------------
Text To Database
----------------

Este proyecto consiste en analizar, preprocesar, generar los niveles MCER y generar vocabulario de cada uno de los textos del corpus ubicado en "Corpus/Corpus.txt". Los elementos generados se insertarán en una base de datos construida con el DBMS PostgreSQL.

----------
COMENZANDO
----------

Pre-requisitos

Para obtener una copia del proyecto, se debe solicitar al desarrollador Carlos Quintero (cquintero.xzv@icloud.com) u obtener una copia del proyecto desde Bitbucket, cuando sea de dominio público, mediante la instrucción Git:

git clone git@bitbucket.org:karlhos89/texttodatabase.git

Es necesario tener un entorno de desarrollo con las siguientes características como mínimo:

- Ruby 2.6.3p62
- Gem PG 1.1.4
- PostgreSQL 11

Instalación

Ruby se instaló mediante RVM 1.29.7. Más información sobre cómo instalar RVM en:
http://rvm.io/

PostgreSQL se instaló con los binarios creados en su sitio oficial:
https://www.postgresql.org/download/

Gem PG se instaló después de la correcta instalación de Ruby y PostgreSQL, se utilizó el comando:
gem install pg

----------
DESPLIEGUE
----------

Una vez el entorno de desarrollo instalado y con una copia del proyecto. Seguir estos pasos:

Base de datos:

Nota: se puede crear la base de datos desde el proyecto Ruby On Rail del SAIC o desde otro proyecto siguiendo el "Modelo de Datos", esto permitirá que sea ActiveRecord quien administre las conexiones (así se construyeron en SAIC). Si se hace manualmente, se deberán cambiar los queries por sus valores apropiados (Tablas y atributos respetando el case_sensitive).

1. Entrar a pgAdmin o al shell de PostgreSQL.
2. Crear un usuario o utiliza el usuario "postgres" por defecto.
3. Crear una base de datos con el query de ejemplo:
CREATE DATABASE nombre_basedatos WITH OWNER = nombre_propietario ENCODING = 'UTF8' CONNECTION LIMIT = -1;
4. Ingresar los queries del archivo "Queries Tablas".
5. Ingresar los queries del archivo "Queries Relaciones".

Ejecución del código Ruby:

1. Abrir la Terminal (línea de comandos de tu sistema operativo).
2. Dirijete hasta la carpeta del proyecto "Text To Database".
3. En la carpeta del proyecto, escribe y ejecuta el siguiente comando para poner en marcha el proyecto:

ruby TextToDatabase.rb

El resultado es un despliegue de características procesadas en la Terminal y la inserción de todos los componentes en la base de datos creada en el DBMS PostgreSQL.

------------------
INFORMACIÓN CORPUS
------------------

El corpus "Corpus.txt" cuenta con 660 textos, 2640 preguntas (4 preguntas por texto) y 10560 respuestas (4 respuestas por pregunta). Este corpus es una compilación de los archivos:

- mc500.dev.txt
- mc500.train.txt
- mc160.dev.txt
- mc160.train.txt
- mc160.test.txt
- mc500.test.txt

Estos archivos pertenecen al proyecto "MCTest". Este proyecto se encuentra en GitHub: https://github.com/mcobzarenco/mctest. Más información sobre este proyecto puede ser consultada en el artículo "MCTest: A Challenge Dataset for the Open-Domain Machine Comprehension of Text", disponible en línea: https://www.microsoft.com/en-us/research/publication/mctest-challenge-dataset-open-domain-machine-comprehension-text/.

Modificaciones al corpus

Modificación 2019/10/17
Se agregaron palabras destacadas a los textos que no tenían "creativity words". Las líneas agregadas se encuentran bajo el marcador "Creativity Words:" dentro de las líneas de código 13354 - 20406.

Modificación 2019/10/24
Se agregó el perfil MCER a cada texto del corpus. Las líneas agregadas se encuentran bajo el marcador "CEFR:" en todo el archivo.

Información adicional

Las palabras destacadas y el perfil MCER fueron proporcionadas por la herramienta TextAnalyzer: http://www.roadtogrammar.com/textanalysis/. Más información de la herramienta, se puede consultar el archivo "Corpus/TextAnalyzer.txt" o en su sitio web oficial (disponible sólo en inglés).

-------
AUTORES
-------

Ing. Carlos Alberto Peralta Quintero - Tesista de maestría
Dr. Noé Alejandro Castro Sánchez - Director de tesis
Dr. Juan Gabriel González Serna - Co-director de tesis
Dra. Andrea Magadán Salazar - Revisora
Dr. Maximo López Sánchez - Revisor

--------
LICENCIA
--------

Sin licencia. Hasta el 31 de octubre del 2019 este proyecto no es de dominio público.
