#encoding: UTF-8

#Importar archivos Ruby (.rb)
require 'pg'
require 'time'

version = PG.library_version.to_s
version.gsub!('0', '.')

puts "Version of libpg: #{version}"

begin
   # Inicializar variables de conexión
   host = '127.0.0.1' # localhost, colocar dirección, no resuelve dominios
   port = '5432' # puerto por defecto 5432, no acepta otro
   dbms = 'basedatos1'
   user = 'saic_user'
   pass = 'saic_user'
   time = 10

   puts 'DBMS PostgreSQL: Conectando...'

   # Objeto de conexión como HASH
   connection = PG::Connection.new(:hostaddr => host, :port => port, :dbname => dbms, :user => user, :password => pass, :connect_timeout => time)
   puts 'DBMS PostgreSQL: Conexión realizada'


   # INSERT

   codigo = 8
   codigoMateria = 2
   claveUsuario = 1

   # connection.exec("INSERT INTO \"Usuario_Materia\" (\"codigo\", \"codigoMateria\", \"claveUsuario\") VALUES (#{codigo}, #{codigoMateria}, #{claveUsuario})")
   # puts "INSERT INTO \"Usuario_Materia\" (\"codigo\", \"codigoMateria\", \"claveUsuario\") VALUES (#{codigo}, #{codigoMateria}, #{claveUsuario})"

   # Cuando se inserta un string va entre comillas simples: '#{claveUsuario}'


   # UPDATE

   codigo = 8
   codigoMateria = 2
   claveUsuario = 1

   # connection.exec("UPDATE \"Usuario_Materia\" SET \"codigoMateria\" = #{codigoMateria}, \"claveUsuario\" = #{claveUsuario} WHERE \"codigo\"=#{codigo};")
   # puts "UPDATE \"Usuario_Materia\" SET \"codigoMateria\" = #{codigoMateria}, \"claveUsuario\" = #{claveUsuario} WHERE \"codigo\"=#{codigo};"


   # DELETE

   codigo = 8
   # codigoMateria = 2
   # claveUsuario = 1

   # connection.exec("DELETE FROM \"Usuario_Materia\" WHERE \"codigo\"=#{codigo};")
   # puts "DELETE FROM \"Usuario_Materia\" WHERE \"codigo\"=#{codigo};"


   # SELECT

   # data = connection.exec("SELECT \"codigo\", \"codigoMateria\", \"claveUsuario\" FROM \"Usuario_Materia\";")
   # puts "SELECT \"codigo\", \"codigoMateria\", \"claveUsuario\" FROM \"Usuario_Materia\";"

   # i=0
   # data.each do |row|
   #    i += 1
   #    puts "- #{i} -"
   #    puts "codigo:        #{row['codigo'].to_i}"
   #    puts "codigoMateria: #{row['codigoMateria'].to_i}"
   #    puts "claveUsuario:: #{row['claveUsuario'].to_s}"
   # end


   # SELECT JOIN

   # data = connection.exec("SELECT * FROM \"Usuario_Materia\"" +
   #    "JOIN \"Usuario\" ON \"Usuario_Materia\".\"claveUsuario\" = \"Usuario\".\"clave\"" +
   #    "JOIN \"Materia\" ON \"Usuario_Materia\".\"codigoMateria\" = \"Materia\".\"codigo\"" +
   #    "WHERE (\"Usuario\".\"clave\" = 1 AND \"Materia\".\"codigo\" = 2)")
   # puts "SELECT * FROM \"Usuario_Materia\" JOIN \"Usuario\" ON \"Usuario_Materia\".\"claveUsuario\" = \"Usuario\".\"clave\""
   # puts "SELECT \"codigo\",\"codigoMateria\",\"claveUsuario\",\"nombre\",\"apellido\" FROM \"Usuario_Materia\" JOIN \"Usuario\" ON \"Usuario_Materia\".\"claveUsuario\" = \"Usuario\".\"clave\""

   # i=0
   # data.each do |row|
   #    i += 1
   #    puts "- #{i} -"
   #    puts "codigo:::::::: #{row['codigo'].to_i}"
   #    puts "codigoMateria: #{row['codigoMateria'].to_i}"
   #    puts "nombreM::::::: #{row['nombreM'].to_s}"
   #    puts "claveUsuario:: #{row['claveUsuario'].to_s}"
   #    puts "nombre:::::::: #{row['nombre'].to_s}"
   #    puts "apellido:::::: #{row['apellido'].to_s}"
   #    puts "registro:::::: #{row['registro'].to_s}"
   # end
   
   
   # SELECT JOIN - DATETIME

   data = connection.exec("SELECT * FROM \"Usuario_Materia\"" +
      "JOIN \"Usuario\" ON \"Usuario_Materia\".\"claveUsuario\" = \"Usuario\".\"clave\"" +
      "JOIN \"Materia\" ON \"Usuario_Materia\".\"codigoMateria\" = \"Materia\".\"codigo\"" +
      "WHERE (\"Usuario\".\"clave\" = 1 AND \"Materia\".\"codigo\" = 2)")
   # puts "SELECT * FROM \"Usuario_Materia\" JOIN \"Usuario\" ON \"Usuario_Materia\".\"claveUsuario\" = \"Usuario\".\"clave\""
   # puts "SELECT \"codigo\",\"codigoMateria\",\"claveUsuario\",\"nombre\",\"apellido\" FROM \"Usuario_Materia\" JOIN \"Usuario\" ON \"Usuario_Materia\".\"claveUsuario\" = \"Usuario\".\"clave\""

   i=0
   datetime = nil
   data.each do |row|
      i += 1
      puts "- #{i} -"
      puts "codigo:::::::: #{row['codigo'].to_i}"
      puts "codigoMateria: #{row['codigoMateria'].to_i}"
      puts "nombreM::::::: #{row['nombreM'].to_s}"
      puts "claveUsuario:: #{row['claveUsuario'].to_s}"
      puts "nombre:::::::: #{row['nombre'].to_s}"
      puts "apellido:::::: #{row['apellido'].to_s}"
      puts "registro:::::: #{row['registro'].to_s}"
      
      datetime = DateTime.strptime(row['registro'].to_s, '%Y-%m-%d %H:%M:%S') # GEM 'time'
   end
   
   puts datetime # Objteto DateTime
   puts datetime.strftime("Formato PostgreSQL: %Y-%m-%d %H:%M:%S") # Formato
   # Suma de tiempo
   value = 5
   datetime = datetime + 0.6931 # Suma en días
   puts datetime
   puts datetime.strftime("Formato PostgreSQL: %Y-%m-%d %H:%M:%S") # Formato

   # raise 'Excepción' # Lanza una excepción con un message

rescue PG::Error => e
   puts "DBMS PostgreSQL: Error (#{e.message})"
   # retry # This will move control to the beginning of begin
else
   puts "DBMS PostgreSQL: Ejecución sin errores"
ensure
   if connection
      connection.close()
   end
end



# Crear la base de datos para este demo:
# Utilizar los queries disponibles en los archivos:
#    - Queries Tables.txt
#    - Queries Relationship.txt
# Se proporciona una guía de SQL en:
#    - PostgreSQL Guide.txt