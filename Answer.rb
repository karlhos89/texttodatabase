#encoding: UTF-8

class Answer
   @@index = 0

   #Inicializar la clase con un indice
   def initialize()
      @@index = @@index + 1

      #Variables de instancia
      @id             = @@index
      @answer         = ""
      @correct_answer = false

      # puts "¡Objeto Answer #{@id} creado!"
   end

   #Set
   def setAnswer(string)
      @answer = string
   end

   def setCorrectAnswer(bool_value)
      @correct_answer = bool_value
   end


   #Get
   def idInt()
      return @id
   end

   def answerStr()
      return @answer
   end

   def correctAnswerBoo()
      return @correct_answer
   end
end
