#encoding: UTF-8

class TextSection
   @@index = 0 #Variable de clase

   #Inicializar la clase con un indice
   def initialize()
      @@index = @@index + 1

      #Variables de instancia
      @id               = @@index
      @title            = ""
      @text             = ""
      @cefr             = ""
		@tokens           = 0 # Palabras totales, incluso las repetidas
      @types            = 0 # Palabras distintas
      @lemmas           = 0 # Lemas distintos
      @creativity_words = Array.new
      @found_words      = Array.new
      @questions        = Array.new
      @vocabulary       = Array.new
      #WordExpression   = Struct.new(:word,:frequency)

      # puts "¡Objeto TextSection #{@id} creado!"
   end


   #Set
   def setTitle(string)
      @title = string
   end

   def setText(string)
      @text = string
   end

   def setCefr(string)
      @cefr = string
   end

	def setTokens(number)
		@tokens = number
	end
	
	def setTypes(number)
		@types = number
   end
   
   def setLemmas(number)
		@lemmas = number
	end

   def setCreativityWords(array)
      @creativity_words = array
   end

   def setFoundWords(array)
      @found_words = array
   end

   def setQuestions(array)
      @questions = array
   end

   def setVocabulary(vocabulary)
      @vocabulary = vocabulary
   end


   #Get
   def idInt()
      return @id
   end

   def titleStr()
      return @title
   end

   def textStr()
      return @text
   end

   def cefrStr()
      return @cefr
   end

   def tokensInt()
      return @tokens
	end
	
	def typesInt()
		return @types
   end
   
   def lemmasInt()
		return @lemmas
	end

   def creativityWordsArr()
      return @creativity_words
   end

   def foundWordsArr()
      return @found_words
   end

   def questionsArr()
      return @questions
   end

   def vocabularyArr()
      return @vocabulary
   end

end
