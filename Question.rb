#encoding: UTF-8

class Question
   @@index = 0

   #Inicializar la clase con un indice
   def initialize()
      @@index = @@index + 1

      #Variables de instancia
      @id            = @@index
      @question      = ""
      @difficulty    = ""
      @answers       = Array.new

      # puts "¡Objeto Question #{@id} creado!"
   end

   #Set
   def setQuestion(string)
      @question = string
   end

   def setDifficulty(string)
      @difficulty = string
   end

   def addAnswer(answer)
      @answers.insert(-1,answer)
   end

   #Get
   def idInt()
      return @id
   end

   def questionStr()
      return @question
   end

   def difficultyStr()
      return @difficulty
   end

   def answersArr()
      return @answers
   end
end
