#encoding: UTF-8

# Google Cloud Search API
require "rest-client"
require "json"

file = File.open("SAIC-s20200724.json")
string = file.read()
credentials = JSON.parse(string) # Convertir string en json
cx             = credentials["customSearchID"] # ID de Custom Search Engine
key            = credentials["consoleKey"] # Key de Google Cloud Console
search_type    = 'image' # Tipo de búsqueda
number_results = '10' # Número de resultados
word           = 'mexico' # Palabra de búsqueda de imágenes

begin
   puts 'Request: builting...'
   request = RestClient::Request.new(
      method: :get,
      url: "https://customsearch.googleapis.com/customsearch/v1?cx=#{cx}&key=#{key}&searchType=#{search_type}&num=#{number_results}&q=#{word}",
      verify_ssl: true
   )

   puts 'Request: executing...'
   response = request.execute()

   puts 'Request: done!'
   result = JSON.parse(response.to_s)

   # puts "Result: #{result}"
rescue StandardError => exception
   puts "Request: exception (#{exception})"
else
   puts 'Request complete.'
end

items_array = result["items"]
# puts "Items: #{items_array}"

images_array = Array.new()
      
items_array.each do |item|
   image = Hash.new()
   image[:link] = item["link"]
   image_data = item["image"]
   image[:thumbnail_link] = image_data["thumbnailLink"]
   
   images_array << image
end

images_array.each do |image|
   puts "IMAGE"
   puts "Link: #{image[:link]}"
   puts "Thumbnail Link: #{image[:thumbnail_link]}"
end

file.close()