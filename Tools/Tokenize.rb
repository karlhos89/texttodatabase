#encoding: UTF-8

tokens = Array.new()
element = ""
text = "It was then that the fox appeared.
\"Good morning,\" said the fox.
\"Good morning,\" the little prince responded politely, although when he turned around he saw nothing.
\"I am right here,\" the voice said, \"under the apple tree.\"
 \"Who are you?\" asked the little prince, and added, \"You are very pretty to look at.\"
\"I am a fox,\" the fox said."

# if (text.include? "\n")
#    text = text.gsub(/\n/, " # ")
#    puts text
   
#    array = text.split(' ')
#    text = ""
   
#    array.each do |word|
#       word = word.gsub(/\n/, "<br>")
#       puts word
#    end
# end

# return

textArray = text.split(' ')

textArray.each do |substring|
   element = ""
   
   substring.each_char { |char|
      if (element.length == 0) # Si element está vacío, concatenar char
         element.concat(char)
      else # En otro caso...
         
         # Si char es una letra (o acento simple)
         if (char.match(/[a-zA-Z]/) || char.match(/[']/))
            # Si el último caracter de element es una letra (o acento simple)
            if (element[-1, 1].match(/[a-zA-Z]/)  || element[-1, 1].match(/[']/))
               element.concat(char)
            else
               # puts element
               tokens << element
               element = ""
               element.concat(char)
            end
         
         # Si char es un número
         elsif (char.match(/[0-9]/))
            # Si el último caracter de element es un número
            if (element[-1, 1].match(/[0-9]/))
               element.concat(char)
            else
               # puts element
               tokens << element
               element = ""
               element.concat(char)
            end
            
         # En otro caso... (char no es letra ni número)
         else
            # puts element
            tokens << element
            element = ""
            element.concat(char)
         end
         
      end
   }
   
   if (element.length > 0)
      # puts element
      tokens << element
      element = ""
   end
   
end


tokens.each do |token|
   puts token
end
