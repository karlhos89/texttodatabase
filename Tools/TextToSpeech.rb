#encoding: UTF-8

# Google Cloud TextToSpeech API
require 'google/cloud/text_to_speech'

# Exportar como variable global las Credenciales de Aplicación de Google
# export GOOGLE_APPLICATION_CREDENTIALS="/Users/carlosquintero/SAIC-c0aa60f3c448.json"

# Instanciar cliente del API
client = Google::Cloud::TextToSpeech.text_to_speech

# Texto de entrada
synthesis_input = { text: "Hello, my name is Carlos!" }

# Construir la petición de voz:
# Código de lenguaje ("en-US")
# Generador de voz ssml ("neutral")
voice = {
  language_code: "en-US",
  ssml_gender: "NEUTRAL"
}

# Tipo de audio que se requiere de vuelta
audio_config = { audio_encoding: "MP3" }

# Realizar la petición/solicitud TextoAVoz(TextToSpeech):
# Entrada de texto
# Parámetros de voz
# Tipo de archivo de audio
response = client.synthesize_speech(
  input:        synthesis_input,
  voice:        voice,
  audio_config: audio_config
)

# La respuesta de audio es binaria: audio_content
File.open "output.mp3", "wb" do |file|
  # Write the response to the output file.
  file.write response.audio_content
end

puts "El audio (audio_content) se guardó en el archivo: 'output.mp3'"

