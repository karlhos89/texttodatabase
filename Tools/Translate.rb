#encoding: UTF-8

# Google Cloud Translation API
require "google/cloud/translate"

# Exportar como variable global las Credenciales de Aplicación de Google
# export GOOGLE_APPLICATION_CREDENTIALS="/Users/carlosquintero/SAIC-c0aa60f3c448.json"

# ID de proyeto (JSON proporcionado por Google Cloud Platform)
project_id    = "saic-1589757036380"

# Instanciar cliente del API
client = Google::Cloud::Translate.new(version: :v2, project_id: project_id)

# Texto de entrada (nativo) y código de idioma objetivo
text          = "This is a test!"
language_code = "es"

# Invocar servicio al cliente
translation = client.translate(text, to: language_code)

puts "Texto:::::: #{text}"
puts "Traducción: #{translation}"
puts "Traducción de #{translation.from} a #{translation.to}"
