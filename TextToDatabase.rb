#encoding: UTF-8

#Importar archivos Ruby (.rb)
require 'pg' # Controlador Ruby GEM-PostgreSQL
require 'freeling-analyzer'
require_relative 'TextSection'
require_relative 'Question'
require_relative 'Answer'


# * * *   CONSTANTES   * * *
CORPUS_PATH             = 'Corpus/Corpus.txt'

TEXT_SECTION_MARKET     = '***************************************************'
CEFR_MARKER             = 'CEFR:             '
STORY_MARKER            = 'Story:            '
AUTHOR_MARKER           = 'Author:           '
QUAL_SCORE_MARKER       = 'Qual. score:      '
WORK_TIME_MARKER        = 'Work Time(s):     '
CREATIVITY_WORDS_MARKER = 'Creativity Words: '

QUESTION_ONE_MARKER   = '1: '
QUESTION_TWO_MARKER   = '2: '
QUESTION_THREE_MARKER = '3: '
QUESTION_FOUR_MARKER  = '4: '
QUESTION_NORMAL_MARKER = 'one: '
QUESTION_HARD_MARKER   = 'multiple: '
QUESTION_NORMAL_DIFFICULTY = 'Normal'
QUESTION_HARD_DIFFICULTY   = 'Difícil'

ANSWER_ONE_MARKER     = 'A) '
ANSWER_TWO_MARKER     = 'B) '
ANSWER_THREE_MARKER   = 'C) '
ANSWER_FOUR_MARKER    = 'D) '
CORRECT_ANSWER_MARKER  = '*'


# * * *   VARIABLES DE INSTANCIA   * * *

@corpus = Array.new
@text_section

@cefr = "";

@creativity_words = Array.new

@text_capture = false
@text = "";

@questions = Array.new
@question

@answer

# Acceso a los elementos de vocabulary
Expression = Struct.new(:word, :lemma, :word_frequency, :lemma_frequency)



# * * *   FUNCIONES   * * *

# Crear objeto Question e indicar si es dificaultad normal o dificil
def createQuestion(line, marker)
   q = Question.new

   line.slice! marker #Eliminar el marker

   #Determinar dificultad de la pregunta
   if line.index(QUESTION_NORMAL_MARKER)
      # puts ' NORMAL '
      line.slice! QUESTION_NORMAL_MARKER #Eliminar el marker
      q.setDifficulty(QUESTION_NORMAL_DIFFICULTY)

   elsif line.index(QUESTION_HARD_MARKER)
      # puts ' DIFÍCIL '
      line.slice! QUESTION_HARD_MARKER #Eliminar el marker
      q.setDifficulty(QUESTION_HARD_DIFFICULTY)

   end

   q.setQuestion(line)

   return q
end

# Crear objeto Answer e indicar si es correcto o no
def createAnswer(line, marker)
   a = Answer.new

   line.slice! marker #Eliminar el marker

   #Determinar dificultad de la pregunta
   if line.index(CORRECT_ANSWER_MARKER)
      # puts ' CORRECT ANSWER '
      line.slice! CORRECT_ANSWER_MARKER #Eliminar el marker
      a.setCorrectAnswer(true)
   else
      line.slice! ' '
      a.setCorrectAnswer(false)
   end

   a.setAnswer(line)

   return a
end

# Obtener palabras creativas que sí se encuentran en el texto corto
def obtainFoundWords(creativity_words, text)
   normal_text = normalizeText(text)
   found_words = Array.new

   creativity_words.each do |word|
      word.delete!("\n") #elimina el salto de linea
      word.downcase!
      if normal_text.index(" #{word} ")
         # puts "Palabra encontrada: #{word}"
         found_words.insert(-1,word)
      end
   end

   return found_words
end

# Crear título para el texto corto
def createTextTitle(text)
   title = text.split('.')[0] # Obtener oración hasta el punto
   title = title + "."

   title_exclamation = title.split('!')[0] # Obtener oración hasta exclamación
   if (title != title_exclamation)
      title = title_exclamation + "!"
   end

   title_interrogation = title.split('?')[0] # Obtener oración hasta interrogación
   if (title != title_interrogation)
      title = title_interrogation + "?"
   end

   # title = title.delete!('"') # Equivalente a title.delete!('"')
   title.delete!('"') # Eliminar las comillas dobles
   title.delete!("'") # Eliminar las comillas simples

   return title
end

# Obtener el número de palabras del texto corto
def countTokens(text)
   normal_text = normalizeText(text)
   tokens = normal_text.split(' ')
   # puts "Palabras en texto: #{tokens} #{normal_text}"
   # En los textos originales existen contracciones,
   # en la BD se normaliza y reemplazan las contracciones por palabras completas

   return tokens.size
end

# Obtener el número de palabras distintas en el texto (types)
def countTypes(vocabulary)
   return vocabulary.size
end

# Obtener el número de lemas distintos en el texto (lemmas)
def countLemmas(vocabulary)
   lemmas = Array.new()

   # Obtener lemas (sin repetir)
   vocabulary.each do |vocab|
      existed_lemma = false

      # Comprobar si el lema ya existe en el arreglo
      lemmas.each do |lemma|
         if (lemma == vocab.lemma)
            existed_lemma = true
            break
         end
      end

      # Si el lema no existe, entonces insertar
      unless (existed_lemma)
         lemmas.insert(-1, vocab.lemma)
      end

   end

   return lemmas.size
end

 # Crea vocabulario (word, lemma)
def createTextVocabulary(text)
   vocabulary = Array.new()

   normalized_text = normalizeText(text)
   expresions = lemmatizeText(normalized_text)

   # Obtener frecuencia de palabra y vocabulario por palabra única
   expresions.each do |exp|
      existed_exp = false

      # Si existe la palabra se suma a la frecuencia +1
      vocabulary.each do |vocab|
         if (vocab.word == exp.word)
            vocab.word_frequency += 1
            existed_exp = true
            # puts "Frecuencia: #{vocab.word_frequency} #{vocab.word}"
            break
         end
      end

      # Si no existe la palabra, entonces se agrega al array
      unless (existed_exp)
         exp.word_frequency = 1
         vocabulary.insert(-1, exp)
         # puts "Inserta: #{exp.word}"
      end

   end

   # Obtener frecuencia de lemma
   vocabulary.each do |vocab|
      lemma_frequency = 0
      vocabulary_copy = Array.new(vocabulary)

      # Calcular frecuencia sólo de los lemas faltantes
      if (vocab.lemma_frequency == 0)
         vocabulary_copy.each do |aux|
            if (aux.lemma == vocab.lemma)
               lemma_frequency += aux.word_frequency
            end
         end

         vocab.lemma_frequency = lemma_frequency
      end

      # puts "#{vocab.word_frequency} :: #{vocab.lemma_frequency} (#{vocab.word}/#{vocab.lemma})"
   end

   return vocabulary
end

# Normaliza texto
def normalizeText(text)
   nText = text.delete('.') # Remover signos de puntuación
   nText.delete!(',') # Optimiza: nText = nText.delete(',')
   nText.delete!(':')
   nText.delete!(';')
   nText.delete!('?')
   nText.delete!('!')
   nText.delete!('$')
   nText.delete!('(')
   nText.delete!(')')
   nText.delete!('"')
   # nText.downcase!() # Se omite para que freeling detecté los NP (noun|proper)
   nText = replaceContractions(nText) # Reemplazar contracciones
   nText.gsub!("'","") # Remover comilla simple de enfásis
   nText.gsub!("- "," ") # Remover guiones de diálogo o enfásis
   nText.gsub!(" -"," ")

   # puts nText
   return nText
end

# Reemplaza contracciones por palabras completas
def replaceContractions(text)
   text.gsub!("can't","can not")
   text.gsub!("won't","will not")
   text.gsub!("'m"," am")
   text.gsub!("'s"," is")
   text.gsub!("s'","s is")
   text.gsub!("'re"," are")
   text.gsub!("'d"," had")
   text.gsub!("'ve"," have")
   text.gsub!("'ll"," will")
   text.gsub!("n't"," not")
   text.gsub!("'n"," and")

   return text
end

# Lematiza texto (freeling)
def lemmatizeText(text)
   not_valid_chars = ['?', '_'] # Caracteres que devuelve freeling en tokens especiales
   expressions = Array.new()

   # Consulta remota a freeling por servidor local
   analyzer = FreeLing::Analyzer.new(text, :server_host => 'localhost:50005')

   # Obtiene palabras y lemas del ánalisis de freeling (y Tokenizar)
   tokens = analyzer.tokens.map { |token|
      # Ignorar NP (noun|proper)
      e = (token.tag == 'NP') ? nil : Expression.new(token.form, token.lemma, 0, 0)
   }

   # Corrige lemmas de freeling de cada token
   tokens.compact!.each do |token|
      # Ignorar token cuando no es de una palabra
      unless ( isNumber(token.word) )

         # Reemplaza lemas que son números y que deberían ser nombres de números/grados
         if (isNumber(token.lemma))
            # puts "REPLACE: #{token.lemma} WITH #{token.word}"
            token.lemma = token.word
         else # Reemplaza lemas que contienen tokens especiales
            not_valid_chars.each do |char|
               if (token.word.include?(char) || token.lemma.include?(char)) # Sin reemplazo
                  # puts "REPLACE: #{token.lemma} WITH #{token.word}"
                  token.lemma = token.word
               end
            end
         end

         token.word.gsub!("_","-")
         token.lemma.gsub!("_","-")

         # Comprueba valores e insertar
         if (token.word != nil && token.word != nil)
            token.word.downcase!()
            token.lemma.downcase!()

            # puts "token: #{token.word}/#{token.lemma}"

            expressions.insert(-1, token)
         end

      end
   end

   return expressions
end

# Comprueba si un string es un número en una cadena o sólo una cadena con letras
def isNumber(string)
   return string.to_f.to_s == string.to_s || string.to_i.to_s == string.to_s
end

# Modifica comillas simples
def modifySimpleQuotes(text)
   text.strip!
   text.gsub!("'","''")

   return text
end


# EJEMPLO DE FREELING

# text = "Alternatively if we do not want to repeat the first steps that we had already performed we could use the output of the morphological analyzer as input to the tagger"
# analyzer = FreeLing::Analyzer.new(text, :server_host => 'localhost:50005')

# freeling = analyzer.tokens.first
# lemmas = analyzer.tokens.map {|w| w.lemma}

# puts "Token:: #{freeling}"
# puts "Lemas:: #{lemmas}"


# * * *   ANÁLISIS DE LOS COMPONENTES DEL CORPUS   * * *

#Abrir archivo
file = File.open(CORPUS_PATH, 'r', encoding:"UTF-8")

#Leer todo el archivo linea por linea
file.each do |line|
   if line.index(TEXT_SECTION_MARKET)
      # puts ' '
      # puts '* * * * * TEXT SECTION * * * * *'
      # puts ' '

      if @text.size > 0
         @text.strip! # remueve los espacios en blanco al inicio y al final

         vocabulary = createTextVocabulary(@text)

         # puts 'TextSection >> Corpus'
         @text_section = TextSection.new
         @text_section.setCefr(@cefr)
         @text_section.setCreativityWords(@creativity_words)
         @text_section.setFoundWords(obtainFoundWords(@creativity_words, @text))
         @text_section.setTitle(createTextTitle(@text))
         @text_section.setText(@text)
			@text_section.setTokens(countTokens(@text))
         @text_section.setTypes(countTypes(vocabulary))
         @text_section.setLemmas(countLemmas(vocabulary))
         @text_section.setQuestions(@questions)
         @text_section.setVocabulary(vocabulary)
         @corpus.insert(-1,@text_section)

         # puts 'TextSection: nueva instancia'
         @creativity_words = Array.new
         @cefr = ""
         @text = ""
         @questions = Array.new
      end
   elsif line.index(CEFR_MARKER)
      line.slice! CEFR_MARKER
      @cefr = line
   elsif line.index(STORY_MARKER)
      # puts 'STORY'
   elsif line.index(AUTHOR_MARKER)
      # puts 'AUTHOR'
   elsif line.index(QUAL_SCORE_MARKER)
      # puts 'QUAL SCORE'
   elsif line.index(WORK_TIME_MARKER)
      # puts 'WORK TIME'

      #Despues de procesar esta linea, se habilita la captura de texto de lectura
      @text_capture = true

   elsif line.index(CREATIVITY_WORDS_MARKER) #Obtener las Creativity Words
      # puts 'CREATIVITY WORDS MARKER * *'
      # puts ' '
      #Eliminar el marker de la linea
      line.slice! CREATIVITY_WORDS_MARKER
      #Separar las creativity words por coma (,)
      line.split(",").each do |cword|
         @creativity_words.insert(-1,cword)
      end
      # creativityWords.each do |item|
      #   puts "CWords: #{item}"
      # end

   elsif line.index(QUESTION_ONE_MARKER)
      # puts ' '
      # puts 'Question 1'
      @question = createQuestion(line, QUESTION_ONE_MARKER)
      # puts "#{@question.questionStr}"
      # puts ' '

      #En esta linea se deshabilita la captura de texto de lectura
      @text_capture = false

   elsif line.index(QUESTION_TWO_MARKER)
      # puts ' '
      # puts 'Question 2'
      @question = createQuestion(line, QUESTION_TWO_MARKER)
      # puts "#{@question.questionStr}"
      # puts ' '

   elsif line.index(QUESTION_THREE_MARKER)
      # puts ' '
      # puts 'Question 3'
      @question = createQuestion(line, QUESTION_THREE_MARKER)
      # puts "#{@question.questionStr}"
      # puts ' '

   elsif line.index(QUESTION_FOUR_MARKER)
      # puts ' '
      # puts 'Question 4'
      @question = createQuestion(line, QUESTION_FOUR_MARKER)
      # puts "#{@question.questionStr}"
      # puts ' '

   elsif line.index(ANSWER_ONE_MARKER)
      # puts 'A ANSWER'
      @answer = createAnswer(line, ANSWER_ONE_MARKER)
      @question.addAnswer(@answer)
      # puts " #{@answer.answerStr}"

   elsif line.index(ANSWER_TWO_MARKER)
      # puts 'B ANSWER'
      @answer = createAnswer(line, ANSWER_TWO_MARKER)
      @question.addAnswer(@answer)
      # puts " #{@answer.answerStr}"

   elsif line.index(ANSWER_THREE_MARKER)
      # puts 'C ANSWER'
      @answer = createAnswer(line, ANSWER_THREE_MARKER)
      @question.addAnswer(@answer)
      # puts " #{@answer.answerStr}"

   elsif line.index(ANSWER_FOUR_MARKER)
      # puts 'D ANSWER'
      @answer = createAnswer(line, ANSWER_FOUR_MARKER)
      @question.addAnswer(@answer)
      # puts " #{@answer.answerStr}"

      #Insertar la pregunta completa en un arrelgo
      @questions.insert(-1, @question)

   else
      #La variable @text_capture indica si se almacena texto como contenido de la historia o se omite, esta variable se pone en true después del MARKER de cretivity words y antes de la question 1.
      if @text_capture
         # puts 'Add text line'
         @text += line
      end
   end


end

file.close



# * * *   OBTENCIÓN DE CONTENIDOS DEL CORPUS DESDE OBJETOS A BASE DE DATOS   * * *

puts ' * * * '
puts "Textos en el corpus: #{@corpus.size}"
puts ' * * * '



begin
   # Versión de Gem PG para una conexión Ruby/PostgreSQL
   version = PG.library_version.to_s
   version.gsub!('0','.')
   puts "\nVersión de libpg (GEM PostgreSQL): #{version}"

   # Parametros de la conexión
   host = '127.0.0.1'
   port = '5432'
   dbms = 'saic_database_dev' # 'data_database_dev'
   user = 'saic_user'
   pass = 'saic_user'
   time = 10
   query = ''

   # Objeto de conexión, el objeto es un tipo HASH
   puts 'DBMS PostgreSQL: Conectando...'
   connection = PG::Connection.new(:hostaddr => host, :port => port, :dbname => dbms, :user => user, :password => pass, :connect_timeout => time)
   puts "DBMS PostgreSQL: Conexión lista.\n\n"

   numero_texto = 0

   @corpus.each do |text_section|
      numero_texto += 1
      puts "INSERT: Text #{numero_texto} * * * * * * * * * *"

      # La transacción hace un COMMIT si y sólo si TODOS los queries se pueden ejecutar.
      # Si existe algún error con un query, la transacción realiza un ROLLBACK.
      connection.transaction do |connection|

         # TEXTOS

         # Insert Text
         text_id = text_section.idInt()
         title   = modifySimpleQuotes(text_section.titleStr)
         body    = modifySimpleQuotes(text_section.textStr)
			tokens  = text_section.tokensInt()
         types   = text_section.typesInt()
         lemmas  = text_section.lemmasInt()
         query = "INSERT INTO \"texts\" (\"id\", \"title\", \"body\", \"tokens\", \"types\", \"lemmas\", \"created_at\", \"updated_at\") VALUES (#{text_id}, \'#{title}\', \'#{body}\', #{tokens}, #{types}, #{lemmas}, \'2020-04-03 08:45:00\', \'2020-04-03 08:45:00\');"
         # puts query
         connection.exec(query)

         # Obtener profileID
         cerf = text_section.cefrStr.strip!
         query = "SELECT \"id\" FROM \"cefr_profiles\" WHERE \"level\"=\'#{cerf}\';"
         # puts query
         data = connection.exec(query)
         profile_id = 0
         data.each do |row|
            # puts "profileID: #{row['profileID'].to_i}"
            profile_id = row['id'].to_i
         end

         # Insert "ProfiledText": "Text" con "MCERProfile" (Relación)
         query = "INSERT INTO \"profiled_texts\" (\"cefr_profile_id\", \"text_id\", \"created_at\", \"updated_at\") VALUES (#{profile_id}, #{text_id}, \'2020-04-03 08:45:00\', \'2020-04-03 08:45:00\');"
         # puts query
         connection.exec(query)

         # PREGUNTAS Y RESPUESTAS

         text_section.questionsArr.each do |question|
            puts 'INSERT: Question - - - - - '
            # Insert "Question"
            question_id    = question.idInt
            question_text = modifySimpleQuotes(question.questionStr)
            query = "INSERT INTO \"questions\" (\"id\", \"text\", \"created_at\", \"updated_at\") VALUES (#{question_id}, \'#{question_text}\', \'2020-04-03 08:45:00\', \'2020-04-03 08:45:00\');"
            # puts query
            connection.exec(query)

            # Insert "Question" con "Text" (Relación)
            difficulty = modifySimpleQuotes(question.difficultyStr)
            query = "INSERT INTO \"associated_questions\" (\"difficulty\", \"question_id\", \"text_id\", \"created_at\", \"updated_at\") VALUES (\'#{difficulty}\', #{question_id}, #{text_id}, \'2020-04-03 08:45:00\', \'2020-04-03 08:45:00\');"
            # puts query
            connection.exec(query)

            question.answersArr.each do |answer|
               puts 'INSERT: Answer . . . '
               # Insert "Answer"
               answer_id    = answer.idInt
               answer_text = modifySimpleQuotes(answer.answerStr)
               query = "INSERT INTO \"answers\" (\"id\", \"text\", \"created_at\", \"updated_at\") VALUES (#{answer_id}, \'#{answer_text}\', \'2020-04-03 08:45:00\', \'2020-04-03 08:45:00\');"
               # puts query
               connection.exec(query)

               # Insert "Answer" con "Question" (Relación)
               correct_answer = answer.correctAnswerBoo
               query = "INSERT INTO \"associated_answers\" (\"is_correct\", \"answer_id\", \"question_id\", \"created_at\", \"updated_at\") VALUES (#{correct_answer}, #{answer_id}, #{question_id}, \'2020-04-03 08:45:00\', \'2020-04-03 08:45:00\');"
               # puts query
               connection.exec(query)
            end
         end

         # VOCABULARIO

         text_section.vocabularyArr().each do |expression|
            word  = expression.word
            lemma = expression.lemma
            word_frequency  = expression.word_frequency
            lemma_frequency = expression.lemma_frequency

            begin
               # Obtener text_word_id
               query = "SELECT \"id\" FROM \"text_words\" WHERE \"word\"=\'#{word}\';"
               # puts query
               data = connection.exec(query)
               text_word_id = nil
               data.each do |row|
                  # puts "text_word_id: #{row['id'].to_i}"
                  text_word_id = row['id'].to_i
               end

               # No existe la palabra (no se obtiene text_word_id)
               unless (text_word_id)
                  # Insert word
                  puts 'INSERT: Vocabulary word'
                  query = "INSERT INTO \"text_words\" (\"word\", \"lemma\", \"created_at\", \"updated_at\") VALUES (\'#{word}\', \'#{lemma}\', \'2020-04-03 08:45:00\', \'2020-04-03 08:45:00\');"
                  # puts query
                  connection.exec(query)
               end
            end while (! text_word_id) # Se repite mientras NO se obtenga la text_word_id

            # Insert "TextWord" con "Text" (Relación)
            query = "INSERT INTO \"associated_words\" (\"word_frequency\", \"lemma_frequency\", \"top_word\", \"text_word_id\", \"text_id\", \"created_at\", \"updated_at\") VALUES (#{word_frequency}, #{lemma_frequency}, false, #{text_word_id}, #{text_id}, \'2020-04-03 08:45:00\', \'2020-04-03 08:45:00\');"
            # puts query
            connection.exec(query)
         end

         # PALABRAS DESTACADAS

         text_section.foundWordsArr.each do |word|
            # Obtener wordID
            query = "SELECT \"id\" FROM \"text_words\" WHERE \"word\"=\'#{word}\';"
            # puts query
            data = connection.exec(query)
            text_word_id = nil
            data.each do |row|
               # puts "text_word_id: #{row['id'].to_i}"
               text_word_id = row['id'].to_i
            end

            # No existe la palabra (no se obtiene text_word_id)
            if (text_word_id)
               # Update topWord
               puts 'UPDATE: Top Word'
               query = "UPDATE \"associated_words\" SET \"top_word\" = true WHERE (\"text_word_id\"=#{text_word_id} AND \"text_id\"=#{text_id});"
               # puts query
               connection.exec(query)
            else
               puts "Se omite del vocabulario: #{word}"
            end
         end

      end

   end

rescue PG::Error => error
   puts "\n\nDBMS PostgreSQL: Error en la transacción  (#{error.message})."
   # retry # Esta instrucción vuelve a repetir la ejecución del codigo a BEGIN
else
   puts "\n\nDMBS PostgreSQL: Ejecución completa."
ensure
   if (connection)
      connection.close()
   end
end
