#encoding: UTF-8

require_relative '../TRTools'

class UserWord
	attr_accessor :word_id
   attr_accessor :word
   attr_accessor :lemma
   
	attr_accessor :word_es
	attr_accessor :sentence
	attr_accessor :word_image
	attr_accessor :repetitions
	attr_accessor :encounters
	attr_accessor :record_repetition
	attr_accessor :user_first_review
	attr_accessor :user_last_review
	attr_accessor :user_next_review
	attr_accessor :probability
   attr_accessor :text_id
   
	attr_accessor :scores_array
	
	
	## Public: Inicializa el modelo de datos: UserWord (UserVocabulary / InteractedUserWord / User)
	# 
	# id                :: Identificador (integer)
	# word              :: Palabra en inglés (string)
	# word_es           :: Palabra en español (string)
	# sentence          :: Oración en la cual se encontró la palabra en inglés (string)
	# image             :: Imagen representativa de la palabra (base64)
	# repetitions       :: Número de repeticiones correctas y continuas (integer)
	# encounters        :: Número de encuentros con la palabra (integer)
	# record            :: Número de repasos correctos, no consecutivos (integer)
	# user_first_review :: Fecha y hora de la 1a revisión de la palabra (Date) (Format: AAAA-MM-DD)
	# user_last_review  :: Fecha y hora de la última revisión de la palabra (Date)
	# user_next_review  :: Fecha y hora de la siguiente revisión de la palabra (Date)
	# text_id           :: Identificador donde se encontró la palabra (integer)
	# score_array       :: Puntajes TF-IDF de los textos donde se encuentra la palabra (array)
	def initialize(word_id, word, lemma, word_es, sentence, image, repetitions, encounters, record, user_first_review, user_last_review, user_next_review, probability, text_id, scores_array = Array.new())
		@word_id           = word_id
      @word              = word
      @lemma             = lemma
      
		@word_es           = word_es
		@sentence          = sentence
		@word_image        = image
		@repetitions       = repetitions
		@encounters        = encounters
		@record_repetition = record
		@user_first_review = user_first_review
		@user_last_review  = user_last_review
		@user_next_review  = user_next_review
		@probability       = probability
      @text_id           = text_id
      
		# Arreglo
		@scores_array      = scores_array
	end
	
	
	## Public: Calcula un promedio de TF-IDF para los puntajes obtenidos en @scores_array
	#
	def tf_idf()
		summation = 0.0
		
		scores_array.each do |tf_idf|
			summation = summation + tf_idf
		end
		
		# Obtener la media aritmética
		return (summation.to_f / scores_array.size.to_f).round(DECIMALS)
	end
	
end