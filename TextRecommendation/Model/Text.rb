#encondig: UTF-8

class Text
	attr_accessor :id
	attr_accessor :title
	attr_accessor :text
	attr_accessor :tokens
   attr_accessor :types
   attr_accessor :lemmas
   
	attr_accessor :status
	attr_accessor :recommendation
   attr_accessor :recommendation_quality
   
	attr_accessor :cefr_id
	attr_accessor :cefr_level
	attr_accessor :cefr_name
   attr_accessor :cefr_description
   
   attr_accessor :similarity # es recommendation_quality cuando se inserta o actualiza
   
	attr_accessor :vocabulary # Lista < SystemWord >
	attr_accessor :top_words # Lista < SystemWord >
	
	## Public: Inicializa el modelo de datos
	# 
	# id             :: Identificador (integer)
	# title          :: Título del texto (string)
	# text           :: Texto (string)
	# tokens         :: Cantidad de palabras que conforman el texto (int)
	# types          :: Cantidad de palabras diferentes que conforman el texto (int)
	# status         :: Estado del texto: Nuevo / Leído / Revisado / nil (string)
	# recommendation :: Si el texto es recomendado, true. De lo contrario, false (bool)
	# recommendation_quality :: Puntaje de calidad de la recomendación (similitud sem.) (float)
	# cefr_id                :: Identificador del nivel MCER (integer)
	# cefr_level             :: Nivel MCER (string)
	# cefr_name              :: Nombre del nivel MCER (string)
	# cefr_description       :: Descripción del nivel MCER (string)
	# similarity             :: Similitud semántica voc. texto vs voc. usuario (float)
	# vocabulary             :: Vocabulario del texto (array)
	# top_words              :: Lista de palabras que son 'creativity words' (array)
	def initialize(id, title, text, tokens, types, lemmas, status, recommendation, recommendation_quality, cefr_id, cefr_level, cefr_name, cefr_description, similarity, vocabulary = Array.new(), top_words = Array.new())
		@id                     = id
		@title                  = title
		@text                   = text
		@tokens                 = tokens
      @types                  = types
      @lemmas                 = lemmas
      
		@status                 = status
		@recommendation         = recommendation
      @recommendation_quality = recommendation_quality
      
		@cefr_id                = cefr_id
		@cefr_level             = cefr_level
		@cefr_name              = cefr_name
      @cefr_description       = cefr_description
      
      @simularity             = similarity
      
		# Arreglos
		@vocabulary             = vocabulary
		@top_words              = top_words
	end
	
end