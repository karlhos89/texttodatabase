#encondig: UTF-8

class SystemWord
	attr_accessor :id
   attr_accessor :word
   attr_accessor :lemma
   
   attr_accessor :word_frequency # Valor absoluto para todas las palabras iguales de un mismo vocab.
   attr_accessor :lemma_frequency # Valor absoluto para todos los lemas iguales de un mismo vocab.
   attr_accessor :top_word
   
	attr_accessor :tf
	attr_accessor :idf
	attr_accessor :tf_idf
	
	## Public: Inicializa el modelo de datos: SystemWord (SystemVocabulary / AssociatedWord / Text)
	# 
	# id        :: Identificador (integer)
   # word      :: Palabra en inglés (string)
   # lemma     ::
   # word_freq :: Número de veces que aparece la palabra en el texto (integer)
   # lemma_fre ::
	# top_word  :: Indica si la palabra es una creativity word (bool)
	# tf        :: Puntaje TF (float)
	# idf       :: Puntaje IDF (float)
	# tf_idf    :: Puntaje TF-IDF (float)
	def initialize(id, word, lemma, word_frequency, lemma_frequency, top_word, tf, idf, tf_idf)
		@id              = id
      @word            = word
      @lemma           = lemma
      
      @word_frequency  = word_frequency
      @lemma_frequency = lemma_frequency
      @top_word        = top_word
      
		@tf              = tf
		@idf             = idf
		@tf_idf          = tf_idf
	end
	
end