#encoding: UTF-8

require 'pg'   # GEM/Controlador Ruby-PostgreSQL
require 'time' # GEM/Date
require_relative 'Model/User'
require_relative 'Model/Text'
require_relative 'Model/UserWord'
require_relative 'Model/SystemWord'
require_relative 'TRTools'

class TRRequestsAdapter

	## Public: Constructor
	def initialize()
		
	end

	
	## SELECT
   
   
   ## Public: Consulta para obtener los datos del usuario, no incluye el vocabulario
   #
   # email         :: Correo del usuario (string)
	def obtainUser(email)
		puts ' obtainUser'
		user = nil

		query = "SELECT \"users\".\"id\", \"email\", \"first_name\", \"last_name\", \"birthdate\", \"cefr_profiles\".\"id\", \"level\", \"cefr_profiles\".\"name\", \"cefr_profiles\".\"description\", \"genders\".\"id\", \"genders\".\"name\", \"genders\".\"description\", \"learning_styles\".\"id\", \"learning_styles\".\"name\", \"learning_styles\".\"description\" " +
		
		"FROM \"users\" " +

		"JOIN \"profiled_users\" ON \"profiled_users\".\"user_id\" = \"users\".\"id\" " +
		"JOIN \"cefr_profiles\" ON \"cefr_profiles\".\"id\" = \"profiled_users\".\"cefr_profile_id\" " +

		"JOIN \"associated_genders\" ON \"associated_genders\".\"user_id\" = \"users\".\"id\" " +
		"JOIN \"genders\" ON \"genders\".\"id\" = \"associated_genders\".\"gender_id\" " +

		"JOIN \"associated_learning_styles\" ON \"associated_learning_styles\".\"user_id\" = \"users\".\"id\" " +
		"JOIN \"learning_styles\" ON \"learning_styles\".\"id\" = \"associated_learning_styles\".\"learning_style_id\" " +

		"WHERE (\"users\".\"email\" = \'#{email}\');"

		data = requestQuery(query)
      
      data.each_row { |row|
         user_id = row[0].to_s
         email = row[1].to_s
			first_name = row[2].to_s
			last_name = row[3].to_s
         birthday = row[4].to_s
         
			cefr_id = row[5].to_s
			cefr_level = row[6].to_s
			cefr_name = row[7].to_s
         cefr_description = row[8].to_s
         
			gender_id = row[9].to_s
			gender_name = row[10].to_s
         gender_description = row[11].to_s
         
			style_id = row[12].to_s
			style_name = row[13].to_s
         style_description = row[14].to_s
         
         user = User.new(user_id, email, first_name, last_name, birthday, cefr_id, cefr_level, cefr_name, cefr_description, gender_id, gender_name, gender_description, style_id, style_name, style_description)
      }

		return user
	end

   
   ## Public: Consulta para obtener el vocabulario del usuario con un rango de fechas
   #
   # user_id         :: Identificador del usuario (string)
	# inital_datetime :: Fecha y hora inicial para la consulta por periodo (DateTime)
	# final_datetime  :: Fecha y hora final para la consulta por periodo (DateTime)
	def obtainUserVocabularyWithDateRange(user_id, initial_datetime, final_datetime)
		puts ' obtainUserVocabularyWithDateRange'
		vocabulary = Array.new()
		user_word = nil
		
		query = "SELECT \"user_id\", \"user_word_id\", \"text_id\", \"word\", \"lemma\", \"word_es\", \"sentence\", \"image\", \"repetitions\", \"encounters\", \"record\", \"user_first_review\", \"user_last_review\", \"user_next_review\" " +
		
		"FROM \"interacted_words\" " +

		"JOIN \"user_words\" ON \"user_words\".\"id\" = \"interacted_words\".\"user_word_id\" " +
		"JOIN \"users\" ON \"users\".\"id\" = \"interacted_words\".\"user_id\" " +

      "WHERE \"users\".\"id\" = \'#{user_id}\' " +
      "AND (\"interacted_words\".\"user_next_review\" > \'#{initial_datetime.strftime(TIMESTAMP_FORMAT)}\' AND \"interacted_words\".\"user_next_review\" <= \'#{final_datetime.strftime(TIMESTAMP_FORMAT)}\') " +

      "ORDER BY \"user_next_review\" ASC " +
      
      "LIMIT #{VOCABULARY_MAX_SIZE};"
      
		data = requestQuery(query)
      
		data.each do |row|
			user_word_id = row["user_word_id"].to_s
         word = row["word"].to_s
         lemma = row["lemma"].to_s
			word_es = row["word_es"].to_s
			sentence = row["sentence"].to_s
			image = row["image"].to_s
			repetitions = row["repetitions"].to_i
			encounters = row["encounters"].to_i
			record = row["record"].to_i
			user_first_review = row["user_first_review"].to_s
			user_last_review = row["user_last_review"].to_s
			user_next_review = row["user_next_review"].to_s
			text_id = row["text_id"].to_s

			user_word = UserWord.new(user_word_id, word, lemma, word_es, sentence, image, repetitions, encounters, record, user_first_review, user_last_review, user_next_review, text_id, Array.new())

			vocabulary.insert(-1, user_word)
		end

		puts "UserWord obtenidas: #{vocabulary.size}" if (VERBOSE)

		return vocabulary
	end
	
	
	## Public: Consulta para obtener el vocabulario del usuario más antiguo
	#
	# user_id         :: Identificador del usuario (string)
	def obtainOldestUserVocabulary(user_id)
		puts ' obtainOldestUserVocabulary'
		vocabulary = Array.new()
		user_word = nil
		
		query = "SELECT \"user_id\", \"user_word_id\", \"text_id\", \"word\", \"lemma\", \"word_es\", \"sentence\", \"word_image\", \"repetitions\", \"encounters\", \"record_repetition\", \"user_first_review\", \"user_last_review\", \"user_next_review\", \"probability\" " +
		
		"FROM \"interacted_words\" " +

		"JOIN \"user_words\" ON \"user_words\".\"id\" = \"interacted_words\".\"user_word_id\" " +
		"JOIN \"users\" ON \"users\".\"id\" = \"interacted_words\".\"user_id\" " +

      "WHERE \"users\".\"id\" = \'#{user_id}\' " +

      "ORDER BY \"user_next_review\" ASC " +
      
      "LIMIT #{VOCABULARY_MAX_SIZE};"
      
		data = requestQuery(query)
      
		data.each do |row|
			user_word_id = row["user_word_id"].to_s
         word = row["word"].to_s
         lemma = row["lemma"].to_s
			word_es = row["word_es"].to_s
			sentence = row["sentence"].to_s
			image = row["image"].to_s
			repetitions = row["repetitions"].to_i
			encounters = row["encounters"].to_i
			record = row["record"].to_i
			user_first_review = row["user_first_review"].to_s
			user_last_review = row["user_last_review"].to_s
			user_next_review = row["user_next_review"].to_s
			text_id = row["text_id"].to_s

			user_word = UserWord.new(user_word_id, word, lemma, word_es, sentence, image, repetitions, encounters, record, user_first_review, user_last_review, user_next_review, text_id, Array.new())

			vocabulary.insert(-1, user_word)
		end

		puts "UserWord obtenidas: #{vocabulary.size}" if (VERBOSE)

		return vocabulary
	end
   

	## Public: Consulta para obtener una lista de textos filtrados por usuario (id, password, perfil MCER)
	#
	# user_id       :: Identificador del usuario, correo electrónico (string)
	# user_profile  :: Perfil MCER del usuario (string)
	def obtainTextsFiltered(user_id, user_cefr_profile)
		puts ' obtainTextsFiltered'
		list = Array.new()
		text = nil

		query = "SELECT  \"texts\".\"id\", \"title\", \"body\", \"tokens\", \"types\", \"lemmas\", \"status\", \"recommendation\", \"recommendation_quality\", \"cefr_profiles\".\"id\", \"level\", \"name\", \"description\" " +
		
		"FROM \"texts\" " +

		"FULL JOIN \"associated_texts\" ON \"associated_texts\".\"text_id\" = \"texts\".\"id\" " +
		"FULL JOIN \"users\" ON \"users\".\"id\" = \"associated_texts\".\"user_id\" " +

		"FULL JOIN \"profiled_texts\" ON \"profiled_texts\".\"text_id\" = \"texts\".\"id\" " +
		"FULL JOIN \"cefr_profiles\" ON \"cefr_profiles\".\"id\" = \"profiled_texts\".\"cefr_profile_id\" " +

		"WHERE (\"users\".\"id\" = \'#{user_id}\') " +
		"AND (\"associated_texts\".\"status\" = \'unread\') " +
		# "AND (\"cefr_profiles\".\"level\" = \'#{user_cefr_profile}\') "+
			
		"ORDER BY \"texts\".\"id\" ASC;"

		data = requestQuery(query)
      
		data.each_row do |row|
			# puts row
			text_id = row[0].to_s
			title = row[1].to_s
			text = row[2].to_s
			tokens = row[3].to_i
         types = row[4].to_i
         lemmas = row[5].to_i
			status = (row[6] == nil ? "unread" : row[6].to_s)
			recommendation = (row[7].to_s == "t" ? true : false)
			recommendation_quality = (row[8] == nil ? 0.0 : row[8].to_f)
			cefr_id = row[9].to_s
			cefr_level = row[10].to_s
			cefr_name = row[11].to_s
			cefr_description = row[12].to_s

			text = Text.new(text_id, title, text, tokens, types, lemmas, status, recommendation, recommendation_quality, cefr_id, cefr_level, cefr_name, cefr_description, 0.0, Array.new(), Array.new())

			list.insert(-1, text)
		end

		puts "Text obtenidos: #{list.size}"

		return list
	end

	## Public: Consulta para obtener el vocabulario de cada texto; y una lista de palabras destacadas
	#
	# text_id :: Identificador del texto (string)
	def obtainTextVocabulary(text_id)
		# puts ' obtainTextVocabulary'
		vocabulary = Array.new()
		system_word = nil

		query = "SELECT \"text_words\".\"id\", \"word\", \"lemma\", \"word_frequency\", \"lemma_frequency\", \"top_word\" " +
		
		"FROM \"text_words\" " +

		"JOIN \"associated_words\" ON \"associated_words\".\"text_word_id\" = \"text_words\".\"id\" " +
		"JOIN \"texts\" ON \"texts\".\"id\" = \"associated_words\".\"text_id\" " +

		"WHERE \"texts\".\"id\" = #{text_id} " +

		"ORDER BY \"top_word\" DESC;"

		data = requestQuery(query)
      
		data.each do |row|
         word_id = row["id"].to_s
         word = row["word"].to_s
         lemma = row["lemma"].to_s
         word_frequency = row["word_frequency"].to_i
         lemma_frequency = row["lemma_frequency"].to_i
			top_word = (row["top_word"].to_s == "t" ? true : false)

			system_word = SystemWord.new(word_id, word, lemma, word_frequency, lemma_frequency, top_word, 0.0, 0.0, 0.0)

			vocabulary.insert(-1, system_word)
		end

		# puts "SystemWord obtenidas: #{vocabulary.size}"

		return vocabulary
	end
	
	
	## UPDATE / INSERT
	
	
	### Public: Actualizar datos en AssociatedText
	#
	# text    :: Objeto del tipo Texto (Text)
	# user_id :: Identificador del usuario (string)
	def updateAssociatedText(text, user_id)
		# puts ' updateAssociatedText'
		status = text.status.size > 0 ? text.status : TEXT_STATUS_NEW
		
		query = "UPDATE \"AssociatedText\" "+
		
		"SET \"status\" = \'#{status}\', \"recommendation\" = #{text.recommendation}, \"recommendationQuality\" = #{text.recommendation_quality} "+
		
		"WHERE (\"textID\" = #{text.id} AND \"userID\" = \'#{user_id}\');"
		
		pg_result = requestQuery(query)
		
		return pg_result.cmd_tuples() == 0 ? false : true # Comprobar que se alteró mínimo 1 registro
		# puts pg_result.error_message() # Mensaje si hubo algún error en la instrucción
		# puts pg_result.cmd_tuples() # Número de registros afectados por la instrucción
	end
	
	### Public: Insertar datos en AssociatedText
	#
	# text    :: Objeto del tipo Texto (Text)
	# user_id :: Identificador del usuario (string)
	def insertAssociatedText(text, user_id)
		# puts ' insertAssociatedText'
		status = text.status.size > 0 ? text.status : TEXT_STATUS_NEW
		
		query = "INSERT INTO \"AssociatedText\" (\"status\", \"recommendation\", \"recommendationQuality\", \"userID\", \"textID\") "+
			
		"VALUES (\'#{status}\', #{text.recommendation}, #{text.recommendation_quality}, \'#{user_id}\', #{text.id});"
			
		pg_result = requestQuery(query)
		
		return pg_result.cmd_tuples() == 0 ? false : true # Comprobar que se alteró mínimo 1 registro
		# puts pg_result.error_message() # Mensaje si hubo algún error en la instrucción
		# puts pg_result.cmd_tuples() # Número de registros afectados por la instrucción
	end
	
	
	# REQUEST
	
	
	### Public: Realiza una petición a base de datos PostgreSQL
	#
	# query :: Una instrucción SQL válida (string)
	def requestQuery(query)
		# Versión de Gem PG para una conexión Ruby/PostgreSQL
		version = PG.library_version.to_s
		version.gsub!("0", ".")
		puts "\nVersión de libpg (GEM PostgreSQL): #{version}" if (VERBOSE)

		begin
			# Objeto de conexión, el objeto es un tipo HASH
			puts "DBMS PostgreSQL: Conectando..." if (VERBOSE)
			connection = PG::Connection.new(:hostaddr => HOST, :port => PORT, :dbname => DATABASE, :user => USER, :password => PASSWORD, :connect_timeout => TIME_REQUEST)
			puts "DBMS PostgreSQL: Conexión lista.\n" if (VERBOSE)

			puts query if (VERBOSE)
			data = connection.exec(query)

			return data
		rescue PG::Error => error
			puts "\nDBMS PostgreSQL: Error (#{error.message})"
			# retry # This will move control to the beginning of begin
			return nil
		else
			puts "\nDBMS PostgreSQL: Ejecución sin errores\n" if (VERBOSE)
		ensure
			if (connection)
				connection.close()
				puts "DBMS PostgreSQL: Conexión terminada." if (VERBOSE)
			end
		end
	end
	
end