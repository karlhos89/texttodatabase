#encoding: UTF-8

require_relative 'TextRecommendation'

## Obtener los DATOS DEL USUARIO
request_adapter = TRRequestsAdapter.new()
user = request_adapter.obtainUser('carlos.peralta18ca@cenidet.edu.mx')
puts "ID Usuario:: #{user.id}"
puts "Correo:::::: #{user.email}"
puts "Nombre:::::: #{user.first_name}"
puts "Apellido:::: #{user.last_name}"
puts "Nacimiento:: #{user.birthdate}"

puts "ID MCER::::: #{user.cefr_id}"
puts "Nivel::::::: #{user.cefr_level}"
puts "Nombre:::::: #{user.cefr_name}"
puts "Descripción: #{user.cefr_description}"

puts "ID Género::: #{user.gender_id}"
puts "Nombre:::::: #{user.gender_name}"
puts "Descripción: #{user.gender_description}"

puts "ID Estilo::: #{user.style_id}"
puts "Nombre:::::: #{user.style_name}"
puts "Descripción: #{user.style_description}"


## Obtener RECOMENDACIONES DE TEXTO para el usuario
text_recommendation = TextRecommendation.new();
recommended_texts = text_recommendation.generateTextRecommendations(user)


return -1


# Actualizar recomendaciones en la base de datos
text_recommendation.requestUpdateTextRecommendations(recommended_texts, user.id)


# PLANTEAR FORMA MÁS VIABLE DE OBTENER SUGERENCIAS DE TEXTOS DESDE LISTA DE PALABRAS DESTACADAS

# unless (isAlgorithmAvailable)
# 	text_recommendations.obtainRandomWords(user)	
# end
# Si generateTextRecommendation devuelve nil,
# entonces ejecutar el método softenColdStart
# y su respectiva interfas de selección de palabras
# text_recommendations.softenColdStart(user)

