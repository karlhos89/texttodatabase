#encondig: UTF-8

# Parámetros de conexión
HOST         = '127.0.0.1'
PORT         = '5432'
DATABASE     = 'saic_database_dev'
USER         = 'saic_user'
PASSWORD     = 'saic_user'
TIME_REQUEST = 10
VERBOSE      = false # Indica si la ejecución generará log o no

# Parámetros generales
LEMMA_ANALYSIS         = true # Indica si la recomendación de textos se hace mediante lema o palabra
VOCABULARY_TIME        = 7  # Periodo del que se obtienen palabras del vocabulario a estudiar (días)
VOCABULARY_MIN_SIZE    = 5  # Cantidad mínima de palabras con las que trabajará el algoritmoTR
VOCABULARY_MAX_SIZE    = 20 # Cantidad máxima de palabras con las que trabajará el algoritmoTR
RECOMMENDED_TEXT_LIMIT = 10 # Cantidad máxima de textos recomend. que proporcionará el algoritmoTR
REMOVE_STOPWORDS       = true # TRUE: remueve de las stopwords las palabras en el voc. del usuario
STOPWORDS_SCORE_LIMIT  = 0.01 # Límite TF-IDF para obtener una lista de stopwords de los textos
DECIMALS               = 15 # Cantidad de decimales con los que trabajará el algorimo

# Parámetros de formato
TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'
TEXT_STATUS_NEW      = 'unread'
TEXT_STATUS_READED   = 'read'
TEXT_STATUS_REVIEWED = 'Revisado'

# Lista de stopwords, obtenida del NLTK + stopwords del corpus del sistema
STOPWORDS = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', 'should', 'now', 'never', 'without', 'would', 'next', 'even', 'best', 'top', 'bottom', 'go', 'goes', 'going', 'done', 'went', 'someone', 'somebody', 'somewhere', 'something', 'sometimes', 'always', 'back', 'could', 'around', 'really', 'make', 'makes', 'get', 'gets', 'got', 'every', 'everyone', 'everybody', 'everywhere', 'everything', 'like', 'liked', 'mom', 'mommy', 'also', 'ouch', 'one', 'first', 'long', 'longer', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'january', 'february', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december', 'new', 'day', 'days', 'put', 'puts', 'real', 'thought', 'good', 'today', 'home', 'said', 'asked', 'time', 'saw', 'gave', 'looked', 'house', 'big', 'see', 'take', 'come', 'came', 'play', 'made', 'friends', 'wanted', 'happy', 'took']