#encoding: UTF-8

require_relative 'TRRequestsAdapter'

class TextRecommendation

	## Public: Constructor
	def initialize()
		@stopwords = Array.new(STOPWORDS)
		@request_adapter = TRRequestsAdapter.new()
	end

	
	## Public: Genera recomendaciones de texto
	#
	# user :: Datos del usuario (User.rb)
	# @return Devuelve un arreglo <Text>, en caso de error devuelve nil
	def generateTextRecommendations(user)
		puts '* * generateTextRecommendations * *'
		system_word = nil
		user_word = nil
		
		# (1) Obtener VOCABULARIO DEL USUARIO desde base de datos
		puts ' (1) Obtener VOCABULARIO DEL USUARIO desde base de datos'
		initial_datetime = DateTime.now() # Día y hora actual
		final_datetime = initial_datetime + VOCABULARY_TIME
		user.vocabulary = @request_adapter.obtainOldestUserVocabulary(user.id)
      # user.vocabulary = @request_adapter.obtainUserVocabularyWithDateRange(user.id, initial_datetime, final_datetime)
      
		## Obtener RECOMENDACIONES DE TEXTO con vocabulario del usuario desde base de datos
		if (user.vocabulary.size >= VOCABULARY_MIN_SIZE)
			puts " UserVocabulary: Available"

			# (2) Obtener TEXTOS FILTRADOS (Filtro: no leídos ni revisados y MCER=+)
			puts ' (2) Obtener TEXTOS FILTRADOS (Filtro: no leídos)'
			user.texts = @request_adapter.obtainTextsFiltered(user.id, user.cefr_level)
			# Obtener la cantidad de TEXTOS FILTRADOS recuperados
         texts_quantity = user.texts.size
         
			# (3) Obtener VOCABULARIO de los textos filtrados
			puts ' (3) Obtener VOCABULARIO de los textos filtrados'
			user.texts.each do |text|				
				# Obtener vocabulario de cada texto
				text.vocabulary = @request_adapter.obtainTextVocabulary(text.id)
				
				# Obtener top_words de cada texto
				i = 0 # Inicializa el contador
				while (i < text.vocabulary.size)
					system_word = text.vocabulary[i]

					if (system_word.top_word)
						text.top_words.insert(-1, system_word)
					end
					
					i += 1 # Incrementa el contador
				end
				# puts "TopWords: #{text.top_words.size}"
			end
			
			# (4) Remover STOPWORDS que aparezcan en el vocabulario del usuario
			puts ' (4) Remover STOPWORDS que aparezcan en el vocabulario del usuario'
			i = 0 # Inicializa el contador
			while ((i < user.vocabulary.size) && REMOVE_STOPWORDS)
				user_word = user.vocabulary[i]
				
				@stopwords.delete(user_word.word)
				
				i += 1 # Incrementa el contador
			end
			
			# (5) Generar ESPACIO VECTORIAL del vocabulario de cada texto
			puts ' (5) Generar ESPACIO VECTORIAL del vocabulario de cada texto'
			user.texts.each do |text|
				i = 0 # Inicializa el contador
				while (i < text.vocabulary.size)
					system_word = text.vocabulary[i]
					
					unless (@stopwords.member?(system_word.word))						
                  # TF = frec. lema|palabra en el texto / total de lemas|palabras en el texto
                  if (LEMMA_ANALYSIS) # lemma
                     system_word.tf = (system_word.lemma_frequency.to_f / text.lemmas.to_f).round(DECIMALS)
                  else # word
                     system_word.tf = (system_word.word_frequency.to_f / text.tokens.to_f).round(DECIMALS)
                  end
                  
                  # IDF = cantidad de textos / cantidad de textos donde aparece el lema|palabra
                  if (LEMMA_ANALYSIS) # lemma
                     word_encounters = countTextsWithLemma(system_word.lemma, user.texts)
                     system_word.idf = (texts_quantity.to_f / word_encounters.to_f).round(DECIMALS)
                  else # word
                     word_encounters = countTextsWithWord(system_word.word, user.texts)
                     system_word.idf = (texts_quantity.to_f / word_encounters.to_f).round(DECIMALS)
                  end
                  
						# TF*IDF
						system_word.tf_idf = (system_word.tf.to_f * system_word.idf.to_f).round(DECIMALS)
						
						# Guardar datos de vectorización TF*IDF en Text.vocabulary
						text.vocabulary[i] = system_word
                  
						# Obtener STOPWORDs del corpus
						# stopword = system_word.tf_idf < STOPWORDS_LIMIT_SCORE ? "STOPWORD: #{system_word.word}" : nil
						# unless (stopword == nil)
						# 	puts stopword
                  # end
                  
                  # puts "#{system_word.lemma} #{system_word.tf_idf()}"
					end
					
					i += 1 # Incrementa el contador
            end
			end
			
			# (6) Generar ESPACIO VECTORIAL del vocabulario del usuario
			puts ' (6) Generar ESPACIO VECTORIAL del vocabulario del usuario'
			i = 0 # Inicializa el contador
			while (i < user.vocabulary.size)
				user_word = user.vocabulary[i]
					
				user.texts.each do |text|
					system_word = text.vocabulary.find { |sw| sw.word == user_word.word}
					
					if (system_word)
						user_word.scores_array.insert(-1, system_word.tf_idf)
					end
				end
				
				i += 1 # Incrementa el contador
			end
			
			# (7) Calcular SIMILITUD SEMÁNTICA de los vocabularios: usuario vs textos
			puts ' (7) Calcular SIMILITUD SEMÁNTICA de los vocabularios: usuario vs textos'
			
			# Obtener arreglo del espacio vectorial del vocabulario del usuario
			user_vector_space = Array.new()
			user.vocabulary.each do |user_word|
				if (user_word.tf_idf() > 0.0)
					# puts "Palabra agregada: #{user_word.word} score: #{user_word.tf_idf()}"
					user_vector_space.insert(-1,user_word.tf_idf())
				end
			end
			
			# Obtener arreglo del espacio vectorial del vocabulario de los textos
			user.texts.each do |text|
				text_vector_space = Array.new()
				
				text.vocabulary.each do |system_word|
					if (system_word.tf_idf > 0.0)
						# puts "Palabra agregada: #{system_word.word} score: #{system_word.tf_idf}"
						text_vector_space.insert(-1,system_word.tf_idf)
					end
				end

				text.similarity = semanticSimilarity(user_vector_space, text_vector_space)
			end
			
			# (8) Obtener TEXTOS SUGERIDOS con la similitud semántica más alta
			puts ' (8) Obtener TEXTOS SUGERIDOS con la similitud semántica más alta'
			recommended_texts = obtainMostSimilarityTexts(user.texts)
			
			recommended_texts.each do |text|
				puts "Text: #{text.id} Score: #{text.similarity}"
			end
			
			return recommended_texts
			
		## Obtener RECOMENDACIONES DE TEXTO con vocabulario desde lista de palabras destacadas
		else
			puts " UserVocabulary: Unavailable"
			
			return nil
		end
		
		return nil
	end
	
	## Public: Actualiza/Inserta las recomendaciones de texto generadas para el usuario
	#
	# texts   :: Array de textos a los que el usuario tiene acceso (Array<Text>)
	# user_id :: Identificador del usuario (string)
	def requestUpdateTextRecommendations(texts, user_id)
		modified_rows = 0
		
		if (texts == nil)
			puts 'No hay textos recomendados para actualizar'
			return false
		end
		
		texts.each do |text|
			# Actualizar registro
			is_update = @request_adapter.updateAssociatedText(text, user_id)
			
			# No se actualiza el registro si no existe, en ese caso se inserta un registro nuevo
			unless (is_update)
				is_insert = @request_adapter.insertAssociatedText(text, user_id)	
			end
			
			modified_rows = (is_update || is_insert) ? modified_rows += 1 : modified_rows
		end
		
		puts "Recomendaciones de texto creadas y modificadas: #{modified_rows}"
		return modified_rows == 0 ? false : true
	end
	
	
	private
	
	# WORD
	## Private: Contar el número de textos en los que aparece una palabra dentro de una colección
	#
	# texts :: Arreglo de textos (Array<Text>)
	# word  :: Palabra que se va a buscar en la colección (string)
	def countTextsWithWord(word, texts)
		system_word = nil
		count = 0
		
		texts.each do |text|
			system_word = text.vocabulary.find { |sw| sw.word == word } # Primera coincidencia
			# system_word = text.vocabulary.select { |sw| sw.word == word } # Devuelve array de las coincidencias, también utiliza el alias .find_all
			if (system_word)
				count += 1
			end
		end
		
		# puts "Palabra encontrada: #{word} (#{count})"
		return count
   end
   
   # LEMMA
	## Private: Contar el número de textos en los que aparece un lema dentro de una colección
	#
	# texts :: Arreglo de textos (Array<Text>)
	# lemma :: Lema que se va a buscar en la colección (string)
   def countTextsWithLemma(lemma, texts)
      system_word = nil
      count = 0
      
      texts.each do |text|
         system_word = text.vocabulary.find { |sw| sw.lemma == lemma }
         
         if (system_word)
            count += 1
         end
      end
      
      # puts "Lema encontrado: #{lemma} (#{count})"
      return count
   end
   
   
   
	## Private: Calcular similitud semántica: voc. usuario vs voc. texto mediante similitud coseno
	#
	# user_vector_space :: Arreglo de puntajes TF*IDF del vocabulario del usuario (Array<float>)
	# text_vector_space :: Arreglo de puntajes TF*IDF del vocabulario de un texto (Array<float>)
	def semanticSimilarity(user_vector_space, text_vector_space)
		# Crear una copia de los array originales
		user_vspace = Array.new(user_vector_space)
		text_vspace = Array.new(text_vector_space)
		
		# Homologar arreglos
		if (user_vspace.size < text_vspace.size)
			difference = text_vspace.size - user_vspace.size
			user_vspace.concat(Array.new(difference, 0.0))
			# puts "user_vspace: #{user_vspace.size} AND text_vspace: #{text_vspace.size}"
		elsif (user_vspace.size > text_vspace.size)
			difference = user_vspace.size - text_vspace.size
			text_vspace.concat(Array.new(difference, 0.0))
			# puts "text_vspace: #{user_vspace.size} AND user_vspace: #{text_vspace.size}"
		end
		
		# Ecuación de la similitud coseno
		i = 0 # Inicializa el contador
		sum_numerator = 0.0
		user_sum_denominator = 0.0
		text_sum_denominator = 0.0
		
		while (i < user_vspace.size)
			sum_numerator = sum_numerator.to_f + (user_vspace[i].to_f * text_vspace[i].to_f).round(DECIMALS)
			
			user_sum_denominator = user_sum_denominator.to_f + (user_vspace[i].to_f * user_vspace[i].to_f).round(DECIMALS)
			text_sum_denominator = text_sum_denominator.to_f + (text_vspace[i].to_f * text_vspace[i].to_f).round(DECIMALS)
			
			i += 1 # Incrementa el contador
		end
		
		user_sum_denominator = Math.sqrt(user_sum_denominator)
		text_sum_denominator = Math.sqrt(text_sum_denominator)
		# puts "numerator::::: #{sum_numerator}"
		# puts "denominator u: #{user_sum_denominator}"
		# puts "denominator t: #{text_sum_denominator}"
		similarity = (sum_numerator.to_f / (user_sum_denominator.to_f * text_sum_denominator.to_f)).round(DECIMALS)
		# puts "Similitud semántica: #{similarity}"
		
		return similarity
	end
	
	## Private: Obtener los textos con la mejor similitud semántica posible
	#
	# texts :: Arreglo de textos con puntaje de similitud semántica (Array<Text>)
	def obtainMostSimilarityTexts(texts)		
		summation = 0.0
		# most_similarity_text = nil
		
		texts.each do |text|
			# Obtener sumatoria de media aritmética
			summation = (summation.to_f + text.similarity.to_f).round(DECIMALS)
			
			# Asignar parametros de recomendación
			text.recommendation = true
			text.recommendation_quality = text.similarity.to_f
			
			# most_similarity_text = (most_similarity_text == nil) ? text : most_similarity_text
			# most_similarity_text = most_similarity_text.similarity < text.similarity ? text : most_similarity_text
		end
		
		# Media aritmética
		arithmetic_mean = (summation.to_f / texts.size.to_f).round(DECIMALS)
		
		# Ordenar textos por similitud semántica mayor
		sort_texts = texts.sort_by { |text| text.similarity}.reverse
		
		# Obtener textos con similitud arriba de la media aritmética
		sort_texts.delete_if { |text| text.similarity < arithmetic_mean }
		
		# Obtener una cantidad máxima de textos con la mayor similitud
		sort_texts = (sort_texts.size > RECOMMENDED_TEXT_LIMIT) ? sort_texts = sort_texts[0..RECOMMENDED_TEXT_LIMIT-1] : sort_texts
		
		return sort_texts
	end
	
end