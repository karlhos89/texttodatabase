#encoding: UTF-8

require_relative 'SpacedRepetition'

## Obtener los DATOS DEL USUARIO
request_adapter = SRRequestsAdapter.new()
user = request_adapter.obtainUser('carlos.peralta18ca@cenidet.edu.mx')
# puts "ID::::::::::::: #{user.id}"
# puts "Correo::::::::: #{user.email}"
# puts "Nombre::::::::: #{user.first_name}"
# puts "Apellido::::::: #{user.last_name}"
# puts "F. nacimiento:: #{user.birthdate}"

# puts "ID nivel MCER:: #{user.cefr_id}"
# puts "Nivel MCER::::: #{user.cefr_level}"
# puts "Nombre MCER:::: #{user.cefr_name}"
# puts "Desc. MCER::::: #{user.cefr_description}"

# puts "ID género:::::: #{user.gender_id}"
# puts "Género::::::::: #{user.gender_name}"
# puts "Desc. dénero::: #{user.gender_description}"

# puts "ID estilo:::::: #{user.style_id}"
# puts "Estilo aprend.: #{user.style_name}"
# puts "Desc. estilo::: #{user.style_description}"


## Obtener VOCABULARIO DEL USUARIO desde base de datos
user.vocabulary = request_adapter.obtainUserVocabulary(user.id)


## Obtener FECHA DEL PRÓXIMO REPASO
user_word = user.vocabulary[0]
puts "Word ID::::::::: #{user_word.word_id}"
puts "Word:::::::::::: #{user_word.word}"
puts "Lemma::::::::::: #{user_word.lemma}"
puts "Word ES::::::::: #{user_word.word_es}"
puts "Sentence:::::::: #{user_word.sentence}"
puts "Image::::::::::: #{user_word.image}"
puts "Repetitions::::: #{user_word.repetitions}"
puts "Encounters:::::: #{user_word.encounters}"
puts "Record:::::::::: #{user_word.record}"
puts "UserFirstReview: #{user_word.user_first_review}"
puts "UserLastReview:: #{user_word.user_last_review}"
puts "UserNextReview:: #{user_word.user_next_review}"
puts '- - -'

# Calcular el momento del siguiente repaso
spaced_repetition = SpacedRepetition.new()
now_datetime = DateTime.now()
# now_datetime = DateTime.strptime(user_word.user_last_review, '%Y-%m-%d %H:%M:%S')

fav_hour = 18

new_user_word = spaced_repetition.calculateNextReview(user_word, now_datetime, fav_hour, CORRECT_REVIEW)



return -1

# is_update = spaced_repetition.requestUpdateInteractedUserWord(user_word, new_user_word, user.id)



# Hacer una transacción
# INSERT en history
# UPDATE en interacted user word

def demoDateTime()
	# Versión de Gem PG para una conexión Ruby/PostgreSQL
	version = PG.library_version.to_s
	version.gsub!('0','.')
	puts "\nVersión de libpg (GEM PostgreSQL): #{version}"

	begin
		# Parametros de conexión
		host = '127.0.0.1'
		port = '5432'
		dbms = 'basedatos1'
		user = 'sicac_user'
		pass = '0000'
		time = 10
		
		# Objeto de conexión, el objeto es un tipo HASH
		puts 'DBMS PostgreSQL: Conectando...'
		connection = PG::Connection.new(:hostaddr => host, :port => port, :dbname => dbms, :user => user, :password => pass, :connect_timeout => time)
		puts "DBMS PostgreSQL: Conexión lista.\n\n"
		
		data = connection.exec("SELECT * FROM \"Usuario_Materia\"" +
		"JOIN \"Usuario\" ON \"Usuario_Materia\".\"claveUsuario\" = \"Usuario\".\"clave\"" +
			"JOIN \"Materia\" ON \"Usuario_Materia\".\"codigoMateria\" = \"Materia\".\"codigo\"" +
			"WHERE (\"Usuario\".\"clave\" = 1 AND \"Materia\".\"codigo\" = 2);")
		puts "SELECT * FROM \"Usuario_Materia\"" +
			"JOIN \"Usuario\" ON \"Usuario_Materia\".\"claveUsuario\" = \"Usuario\".\"clave\"" +
			"JOIN \"Materia\" ON \"Usuario_Materia\".\"codigoMateria\" = \"Materia\".\"codigo\"" +
			"WHERE (\"Usuario\".\"clave\" = 1 AND \"Materia\".\"codigo\" = 2);"
		
		i=0
		datetime = nil
		data.each do |row|
			i += 1
			puts "\n- #{i} -"
			puts "codigo:::::::: #{row['codigo'].to_i}"
			puts "codigoMateria: #{row['codigoMateria'].to_i}"
			puts "nombreM::::::: #{row['nombreM'].to_s}"
			puts "claveUsuario:: #{row['claveUsuario'].to_s}"
			puts "nombre:::::::: #{row['nombre'].to_s}"
			puts "apellido:::::: #{row['apellido'].to_s}"
			puts "registro:::::: #{row['registro'].to_s}"
			
			datetime = DateTime.strptime(row['registro'].to_s, '%Y-%m-%d %H:%M:%S') # GEM 'time'
		end
		
		puts "\n\n"
		puts datetime # Objteto DateTime
		puts datetime.strftime("Formato PostgreSQL: %Y-%m-%d %H:%M:%S") # Formato
		
		# Suma de tiempo
		days = 0.6931 # Suma en días
		datetime = datetime + days
		puts "\n\n"
		puts "+ #{days} día(s)"
		puts datetime
		puts datetime.strftime("Formato PostgreSQL: %Y-%m-%d %H:%M:%S") # Formato
		
		# Obtener tiempo
		datetime = DateTime.now
		puts "\n\n"
		puts "Día y hora actual"
		puts datetime
		puts datetime.strftime("Formato PostgreSQL: %Y-%m-%d %H:%M:%S") # Formato
		
		# Insertar fecha y hora
		date = datetime.strftime("%Y-%m-%d %H:%M:%S")
		connection.exec("UPDATE \"Usuario\" SET \"registro\" = '#{date}' WHERE \"clave\" = 1")
		
		# raise 'Excepción' # Lanza una excepción con un message
	rescue PG::Error => error
		puts "\n\nDBMS PostgreSQL: Error (#{error.message})"
		# retry # This will move control to the beginning of begin
	else
		puts "\n\nDBMS PostgreSQL: Ejecución sin errores\n"
	ensure
		if (connection)
			connection.close()
		end
	end
end

# demoDateTime()