#encoding: UTF-8

class User
   attr_accessor :id
   attr_accessor :email
	attr_accessor :first_name
	attr_accessor :last_name
   attr_accessor :birthdate
   
	attr_accessor :cefr_id
	attr_accessor :cefr_level
	attr_accessor :cefr_name
   attr_accessor :cefr_description
   
	attr_accessor :gender_id
	attr_accessor :gender_name
   attr_accessor :gender_description
   
	attr_accessor :style_id
	attr_accessor :style_name
   attr_accessor :style_description
   
	attr_accessor :vocabulary # Lista < UserWord >
	attr_accessor :texts # Lista < Text >
	
	## Public: Inicializa el modelo de datos: User
	def initialize(id, email, first_name, last_name, birthdate, cefr_id, cefr_level, cefr_name, cefr_description, gender_id, gender_name, gender_description, style_id, style_name, style_description, vocabulary = Array.new(), texts = Array.new())
      @id                 = id
      @email              = email
		@first_name         = first_name
		@last_name          = last_name
		@birthdate          = birthdate
		
		@cefr_id            = cefr_id
		@cefr_level         = cefr_level
		@cefr_name          = cefr_name
      @cefr_description   = cefr_description
      
		@gender_id          = gender_id
		@gender_name        = gender_name
      @gender_description = gender_description
      
		@style_id           = style_id
		@style_name         = style_name
		@style_description  = style_description
		# Arreglos
		@vocabulary         = vocabulary
		@texts              = texts
	end
	
end