#encoding: UTF-8

class SRData
	attr_accessor :repetitions
	attr_accessor :encounters
	attr_accessor :record
	attr_accessor :user_last_review
	attr_accessor :user_next_review
	
	# Public: Inicializa el modelo de datos
	# 
	# repetitions_number :: Número de repasos consecutivos y correctos de una palabra (integer)
	# encounters_number  :: Número de encuentros con una palabra (integer)
	# record             :: Número de repasos correctos, no consecutivos (integer)
	# user_last_review   :: Fecha y hora del (último) repaso de la palabra (string)
	# user_next_review   :: Fecha y hora del próximo repaso de la palabra (string)
	def initialize(repetitions_number, encounters_number, record, user_last_review, user_next_review)
		@repetitions      = repetitions_number
		@encounters       = encounters_number
		@record           = record
		@user_last_review = user_last_review # %Y-%m-%d %H:%M:%S
		@user_next_review = user_next_review # %Y-%m-%d %H:%M:%S
   end
	
end
