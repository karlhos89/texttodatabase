#encoding: UTF-8

# Parámetros de conexión
HOST         = '127.0.0.1'
PORT         = '5432'
DATABASE     = 'saic_database_dev'
USER         = 'saic_user'
PASSWORD     = 'saic_user'
TIME_REQUEST = 10
VERBOSE      = true # Indica si la ejecución generará log o no

# Parámetros generales
PROBABILITY    = 0.5 # Porcentaje de probabilidad para la ecuación de la curva del olvido
DECIMALS       = 15  # Cantidad de decimales con los que trabajará el algorimo
CORRECT_REVIEW = true
WRONG_REVIEW   = false

