#encoding: UTF-8

require_relative 'SRRequestAdapter'
require_relative 'Model/SRData'

class SpacedRepetition
	
	# Public: Constructor
	def initialize()
		@request_adapter = SRRequestsAdapter.new()
	end
	
	
   # Public: Calcular la fecha y hora de la siguiente revisión de una palabra
   #
   # fav_hour :: Corresponde a la hora favorita del usuario/estudiante para realizar un repaso
   def calculateNextReview(user_word, now_datetime, fav_hour, correct_review)
      # repetitions, encounters, record, user_last_review, correct_review
		# Si el repaso fue correcto: repetition++. De lo contrario, repetition=1
		user_word.repetitions = correct_review ? user_word.repetitions += 1 : user_word.repetitions = 1
		puts "Repetitions:::: #{user_word.repetitions}" if (VERBOSE)
		
		# Si el repaso fue correcto o no, no importa: encounters++
		user_word.encounters += 1
		puts "Encounters::::: #{user_word.encounters}" if (VERBOSE)
		
		# Si el repaso fue correcto: record++, sino record queda con el mismo valor
		user_word.record = correct_review ? user_word.record += 1 : user_word.record
		puts "Record::::::::: #{user_word.record}" if (VERBOSE)
      
      # Repetición espaciada normal o ajustada
      if (fav_hour == -1) # normal
         puts 'normal'
         # Ecuación: t = -s (ln p), determina el tiempo entre el repaso actual y el siguiente
         # t: tiempo entre el repaso actual y el siguiente
         # s: número de repasos/repeticiones
         # p: probabilidad de mínimo 50% de recordar la información
         time = ( - user_word.repetitions * Math.log(PROBABILITY) ).round(DECIMALS)
         puts "Intervalo t:::: #{time} días *" if (VERBOSE)
         puts "Probabilidad::: #{PROBABILITY}" if (VERBOSE)
      else # ajustada
         puts 'ajustada'
         # Ecuación: t = -s (ln p), determina el tiempo entre el repaso actual y el siguiente
         # t: tiempo entre el repaso actual y el siguiente
         # s: número de repasos/repeticiones
         # p: probabilidad de mínimo 50% de recordar la información
         time = ( - user_word.repetitions * Math.log(PROBABILITY) ).to_i
         # Filtrar primera repetición: time == 0
         time = (time == 0) ? 1 : time
         puts "Intervalo ti::: #{time} días" if (VERBOSE)
         
         user_word.probability = Math.exp(- time.to_f / user_word.repetitions.to_f)
         puts "Probabilidad::: #{user_word.probability} *" if (VERBOSE)
      end
		
      # Cálculo del siguiente repaso
      last_review = DateTime.strptime(user_word.user_last_review, '%Y-%m-%d %H:%M:%S')      
      next_review = last_review + time
      
		puts "UserLastReview: #{last_review.strftime("%Y-%m-%d %H:%M:%S")}" if (VERBOSE)
		puts "UserNextReview: #{next_review.strftime("%Y-%m-%d %H:%M:%S")}" if (VERBOSE)
      
      user_word.user_last_review = last_review.strftime("%Y-%m-%d %H:%M:%S")
      user_word.user_next_review = next_review.strftime("%Y-%m-%d %H:%M:%S")
      
		return user_word
	end

	## Public: Actualiza la fecha y hora del siguiente repaso de una palabra
	#
	# old_user_word :: Palabra desactualizada del vocabualario del estudiante (UserWord)
	# new_user_word :: Palabra a actualizar del vocabualario del estudiante (UserWord)
	# user_id   :: Identificador del usuario (string)
	def requestUpdateInteractedUserWord(old_user_word, new_user_word, user_id)
		modified_rows = 0
		
		is_update = @request_adapter.updateInteractedUserWord(old_user_word, new_user_word, user_id)
		
		puts "Actualización: #{is_update ? "realizada" : "no realizada"}"
		
		return is_update
	end
end