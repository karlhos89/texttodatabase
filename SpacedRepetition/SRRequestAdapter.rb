#encoding: UTF-8

require 'pg'   # GEM/Controlador Ruby-PostgreSQL
require 'time' # GEM/Date
require_relative 'Model/User'
require_relative 'Model/UserWord'
require_relative 'SRTools'

class SRRequestsAdapter

	## Public: Constructor
	def initialize()
		
	end

	
	## SELECT
	
	
	## Public: Consulta para obtener los datos del usuario, no incluye el vocabulario
	def obtainUser(email)
		puts ' obtainUser'
		user = nil

		query = "SELECT \"users\".\"id\", \"email\", \"first_name\", \"last_name\", \"birthdate\", \"cefr_profiles\".\"id\", \"level\", \"cefr_profiles\".\"name\", \"cefr_profiles\".\"description\", \"genders\".\"id\", \"genders\".\"name\", \"genders\".\"description\", \"learning_styles\".\"id\", \"learning_styles\".\"name\", \"learning_styles\".\"description\" " +
		
		"FROM \"users\" " +

		"JOIN \"profiled_users\" ON \"profiled_users\".\"user_id\" = \"users\".\"id\" " +
		"JOIN \"cefr_profiles\" ON \"cefr_profiles\".\"id\" = \"profiled_users\".\"cefr_profile_id\" " +

		"JOIN \"associated_genders\" ON \"associated_genders\".\"user_id\" = \"users\".\"id\" " +
		"JOIN \"genders\" ON \"genders\".\"id\" = \"associated_genders\".\"gender_id\" " +

		"JOIN \"associated_learning_styles\" ON \"associated_learning_styles\".\"user_id\" = \"users\".\"id\" " +
		"JOIN \"learning_styles\" ON \"learning_styles\".\"id\" = \"associated_learning_styles\".\"learning_style_id\" " +

		"WHERE (\"users\".\"email\" = \'#{email}\');"

		data = requestQuery(query)
      
      data.each_row { |row|
         user_id = row[0].to_s
         email = row[1].to_s
			first_name = row[2].to_s
			last_name = row[3].to_s
         birthday = row[4].to_s
         
			cefr_id = row[5].to_s
			cefr_level = row[6].to_s
			cefr_name = row[7].to_s
         cefr_description = row[8].to_s
         
			gender_id = row[9].to_s
			gender_name = row[10].to_s
         gender_description = row[11].to_s
         
			style_id = row[12].to_s
			style_name = row[13].to_s
         style_description = row[14].to_s
         
         user = User.new(user_id, email, first_name, last_name, birthday, cefr_id, cefr_level, cefr_name, cefr_description, gender_id, gender_name, gender_description, style_id, style_name, style_description)
      }

		return user
	end

	## Public: Consulta para obtener el vocabulario del usuario
	#
	def obtainUserVocabulary(user_id)
		puts ' obtainUserVocabularyWithDateRange'
		vocabulary = Array.new()
		user_word = nil
		
		query = "SELECT \"user_id\", \"user_word_id\", \"text_id\", \"word\", \"lemma\", \"word_es\", \"sentence\", \"word_image\", \"repetitions\", \"encounters\", \"record_repetition\", \"user_first_review\", \"user_last_review\", \"user_next_review\", \"probability\" " +
		
		"FROM \"interacted_words\" " +

		"JOIN \"user_words\" ON \"user_words\".\"id\" = \"interacted_words\".\"user_word_id\" " +
		"JOIN \"users\" ON \"users\".\"id\" = \"interacted_words\".\"user_id\" " +

		"WHERE \"users\".\"id\" = \'#{user_id}\' "+

		"ORDER BY \"user_next_review\" ASC;"
      
      
		data = requestQuery(query)
      
      
		data.each do |row|
			user_word_id = row["user_word_id"].to_s
         word = row["word"].to_s
         lemma = row["lemma"].to_s
			word_es = row["word_es"].to_s
			sentence = row["sentence"].to_s
			image = row["image"].to_s
			repetitions = row["repetitions"].to_i
			encounters = row["encounters"].to_i
			record = row["record"].to_i
			user_first_review = row["user_first_review"].to_s
			user_last_review = row["user_last_review"].to_s
			user_next_review = row["user_next_review"].to_s
			text_id = row["text_id"].to_s

			user_word = UserWord.new(user_word_id, word, lemma, word_es, sentence, image, repetitions, encounters, record, user_first_review, user_last_review, user_next_review, text_id, Array.new())

			vocabulary.insert(-1, user_word)
		end

		puts "UserWord obtenidas: #{vocabulary.size}" if (VERBOSE)

		return vocabulary
	end
	
	
	## UPDATE / INSERT
	
	
	## Public: Actualizar datos en InteractedUserWord e Inserta en UserReviewHistory
	#
	# old_user_word :: Palabra desactualizada del vocabualario del estudiante (UserWord)
	# new_user_word :: Palabra a actualizar del vocabualario del estudiante (UserWord)
	# user_id   :: Identificador del usuario (string)
	def updateInteractedUserWord(old_user_word, new_user_word, user_id)
		# puts ' updateInteractedUserWord'
		queries = Array.new()
		
		# SELECT InteractedUserWord (obtener interactedUserWordID de la palabra a actualizar)
		query_s = "SELECT \"interactedUserWordID\" "+
		
		"FROM \"InteractedUserWord\" "+
		
		"WHERE (\"userWordID\" = #{new_user_word.id} AND \"userID\" = \'#{user_id}\');"
		
		data = requestQuery(query_s)
		
		iuw_id = 0
		
		data.each do |row|
			iuw_id = row["interactedUserWordID"].to_s
		end
				
		# INSERT UserReviewHistory
		# ¿Repaso en tiempo?
		# El repaso debe hacerse antes del limite de la fecha y hora sugerida
		next_review = DateTime.strptime(old_user_word.user_next_review, '%Y-%m-%d %H:%M:%S')
		review = DateTime.strptime(new_user_word.user_last_review, '%Y-%m-%d %H:%M:%S')
		is_in_time = next_review > review
		
		# ¿Repaso correcto?
		# Las repeticiones de la actualización deben ser mayores cuando el repaso fue correcto
		is_correct_review = new_user_word.repetitions > old_user_word.repetitions
				
		query_h = "INSERT INTO \"UserReviewHistory\" (\"userReview\", \"repetitions\", \"userLastReview\", \"userNextReview\", \"isInTime\", \"interactedUserWordID\", \"isCorrectReview\") "+
		
		"VALUES (\'#{new_user_word.user_last_review}\', #{old_user_word.repetitions}, \'#{old_user_word.user_last_review}\', \'#{old_user_word.user_next_review}\', #{is_in_time}, #{iuw_id}, #{is_correct_review});"
		
		queries.insert(-1, query_h)
		
		# UPDATE InteractedUserWord
		query_u = "UPDATE \"InteractedUserWord\" SET "
		query_u = (new_user_word.word_es != nil) ?
						query_u+"\"wordES\" = \'#{new_user_word.word_es}\', " : query_u
		query_u = (new_user_word.sentence != nil) ?
						query_u+"\"sentence\" = \'#{new_user_word.sentence}\', " : query_u
		query_u = (new_user_word.image != nil) ?
						query_u+"\"image\" = \'#{new_user_word.image}\', " : query_u
		query_u = (new_user_word.repetitions != nil) ?
						query_u+"\"repetitions\" = #{new_user_word.repetitions}, " : query_u
		query_u = (new_user_word.encounters != nil) ?
						query_u+"\"encounters\" = #{new_user_word.encounters}, " : query_u
		query_u = (new_user_word.record != nil) ?
						query_u+"\"record\" = #{new_user_word.record}, " : query_u
		query_u = (new_user_word.user_first_review != nil) ?
						query_u+"\"userFirstReview\" = \'#{new_user_word.user_first_review}\', " : query_u
		query_u = (new_user_word.user_last_review != nil) ?
						query_u+"\"userLastReview\" = \'#{new_user_word.user_last_review}\', " : query_u
		query_u = (new_user_word.user_next_review != nil) ?
						query_u+"\"userNextReview\" = \'#{new_user_word.user_next_review}\', " : query_u
		query_u = query_u.delete_suffix(', ')+" WHERE (\"userWordID\" = #{new_user_word.id} AND \"userID\" = \'#{user_id}\');"
		
		queries.insert(-1, query_u)
		
		
		modified_rows = requestTransaction(queries)
				
		return modified_rows == 0 ? false : true # Comprobar que se alteró mínimo 1 registro
	end
	
	
	# REQUEST
	
	
	## Public: Realiza una petición a base de datos PostgreSQL
	#
	# query        :: Una instrucción SQL válida (string)
	# @return data :: Uso general, capaz de devolver datos recuperados
	def requestQuery(query)
		# Versión de Gem PG para una conexión Ruby/PostgreSQL
		version = PG.library_version.to_s
		version.gsub!("0", ".")
		puts "\nVersión de libpg (GEM PostgreSQL): #{version}" if (VERBOSE)

		begin
			# Objeto de conexión, el objeto es un tipo HASH
			puts "DBMS PostgreSQL: Conectando..." if (VERBOSE)
			connection = PG::Connection.new(:hostaddr => HOST, :port => PORT, :dbname => DATABASE, :user => USER, :password => PASSWORD, :connect_timeout => TIME_REQUEST)
			puts "DBMS PostgreSQL: Conexión lista.\n" if (VERBOSE)

			puts query if (VERBOSE)
			data = connection.exec(query)

			return data
			
		rescue PG::Error => error
			puts "\nDBMS PostgreSQL: Error (#{error.message})"
			# retry # This will move control to the beginning of begin
			return nil
		else
			puts "\nDBMS PostgreSQL: Ejecución sin errores\n" if (VERBOSE)
		ensure
			if (connection)
				connection.close()
				puts "DBMS PostgreSQL: Conexión terminada." if (VERBOSE)
			end
		end
	end
	
	## Public: Realiza una transacción a base de datos PostgreSQL
	#
	# queries      :: Un array con instrucciones SQL válidas (Array<string>)
	# @return bool :: Realiza una transacción de INSERT, UPDATE o DELETE, devuelve
	def requestTransaction(queries)
		# Versión de Gem PG para una conexión Ruby/PostgreSQL
		version = PG.library_version.to_s
		version.gsub!("0", ".")
		puts "\nVersión de libpg (GEM PostgreSQL): #{version}" if (VERBOSE)
		
		modified_rows = 0
		
		begin
			# Objeto de conexión, el objeto es un tipo HASH
			puts "DBMS PostgreSQL: Conectando..." if (VERBOSE)
			connection = PG::Connection.new(:hostaddr => HOST, :port => PORT, :dbname => DATABASE, :user => USER, :password => PASSWORD, :connect_timeout => TIME_REQUEST)
			puts "DBMS PostgreSQL: Conexión lista.\n" if (VERBOSE)
			
			connection.transaction do |connection|
				
				queries.each do |query|
					puts query if (VERBOSE)
					pg_result = connection.exec(query)
					
					modified_rows = modified_rows + pg_result.cmd_tuples().to_i
					# puts pg_result.cmd_tuples() # Número de registros afectados por la instrucción
					# puts pg_result.error_message() # Mensaje si hubo algún error en la instrucción
				end
				
			end
			
			return modified_rows
			
		rescue PG::Error => error
			puts "\nDBMS PostgreSQL: Error (#{error.message})"
			# retry # This will move control to the beginning of begin
			return 0
		else
			puts "\nDBMS PostgreSQL: Ejecución sin errores\n" if (VERBOSE)
		ensure
			if (connection)
				connection.close()
				puts "DBMS PostgreSQL: Conexión terminada." if (VERBOSE)
			end
		end
	end
	
end